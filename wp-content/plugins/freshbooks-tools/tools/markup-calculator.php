<?php
include_once dirname(__DIR__) . '/inject-fonts.php';

function freshbooks_markup_calculator( $atts, $content = null ) {
  // Enqueue the registered styles
  wp_enqueue_style( 'freshbooks-tools-plugin-styles' );

  //inject fonts
  inject_fonts('freshbooks-tools','freshbooks-tools-plugin-styles');

  // Enqueue the registered react scripts
  wp_enqueue_script( 'freshbooks-tools-main-react-script' );
  wp_enqueue_script( 'freshbooks-tools-runtime-react-script' );
  wp_enqueue_script( 'freshbooks-tools-static-react-script' );

  // Get prerendered html for tool
  $str = file_get_contents(dirname(__DIR__) . '/json/MarkupCalculator.json');
  $json = json_decode($str, true);
  $html = $json['renderedHTML'];
  $css = $json['css'];

   // Define defaults for allowed attributes
  $a = shortcode_atts( array(
	  'title' => 'Enter your markup information',
	  'subtitle' => 'To get started, enter two of the fields below and we will automatically calculate the rest.'
  ), $atts );

  // Inject short-code attributes into html
  foreach ($a as $prop_name => $prop_value) {
    $search_string = "{{ " . $prop_name . " }}";

    $html = str_replace($search_string, $prop_value, $html);
  };

  $content = $html;

  return $content;
}

add_shortcode('freshbooks_markup_calculator', 'freshbooks_markup_calculator');