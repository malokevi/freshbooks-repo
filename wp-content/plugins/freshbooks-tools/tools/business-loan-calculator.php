<?php
include_once dirname(__DIR__) . '/inject-fonts.php';

function freshbooks_business_loan_calculator( $atts, $content = null ) {
  // Enqueue the registered styles
  wp_enqueue_style( 'freshbooks-tools-plugin-styles' );

  //inject fonts
  inject_fonts('freshbooks-tools','freshbooks-tools-plugin-styles');

  // Enqueue the registered react scripts
  wp_enqueue_script( 'freshbooks-tools-main-react-script' );
  wp_enqueue_script( 'freshbooks-tools-runtime-react-script' );
  wp_enqueue_script( 'freshbooks-tools-static-react-script' );

  // Get prerendered html for tool
  $str = file_get_contents(dirname(__DIR__) . '/json/BusinessLoanCalculator.json');
  $json = json_decode($str, true);
  $html = $json['renderedHTML'];
  $css = $json['css'];

   // Define defaults for allowed attributes
  $a = shortcode_atts( array(
	  'title' => 'Enter your loan information',
	  'results_title' => 'How much will your loan cost',
	  'input_amount_label' => 'Loan amount ($)',
	  'input_rate_label' => 'Annual Interest Rate (%)',
	  'input_term_label' => 'Length of term',
	  'input_extra_payment_label' => 'Extra Monthly Payment ($)'
  ), $atts );

  // Inject short-code attributes into html
  foreach ($a as $prop_name => $prop_value) {
    $search_string = "{{ " . $prop_name . " }}";

    $html = str_replace($search_string, $prop_value, $html);
  };

  $content = $html;

  return $content;
}

add_shortcode('freshbooks_business_loan_calculator', 'freshbooks_business_loan_calculator');