<?php
/**
 * Plugin Name: Freshbooks tools
 * Plugin URI:
 * Description: Display content using a shortcode to insert in a page or post
 * Version: 1.0
 * Text Domain:
 * Author: Freshbooks
 * Author URI:
 */


function plugin_assets() {
  $asset_manifest_str = file_get_contents(dirname(__DIR__) . '/freshbooks-tools/asset-manifest.json');
  $asset_manifest_json = json_decode($asset_manifest_str, true);

  $main_js_path = $asset_manifest_json['main.js'];
  $runtime_js_path = $asset_manifest_json['runtime~main.js'];
  $static_js_path = '';
  $styles_path = $asset_manifest_json['main.css'];

  foreach ($asset_manifest_json as $index=>$asset) {
    if (preg_match('/^(static\/js).+(.js)$/', $index)) {
      $static_js_path = $asset_manifest_json[$index];
    }
  }

  $dir = plugins_url();

  // Register  plugin scripts
  wp_register_script( 'freshbooks-tools-main-react-script', $dir . '/freshbooks-tools' . $main_js_path);
  wp_register_script( 'freshbooks-tools-runtime-react-script', $dir . '/freshbooks-tools' . $runtime_js_path);
  wp_register_script( 'freshbooks-tools-static-react-script', $dir . '/freshbooks-tools' . $static_js_path);

  // Register plugin styles
  wp_register_style( 'freshbooks-tools-plugin-styles', $dir . '/freshbooks-tools' .  $styles_path);

}

add_action( 'wp_enqueue_scripts', 'plugin_assets' );

// Register all tool shortcodes
require(dirname(__DIR__) . '/freshbooks-tools/tools/business-loan-calculator.php');
require(dirname(__DIR__) . '/freshbooks-tools/tools/markup-calculator.php');