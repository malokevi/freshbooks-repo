(window.webpackJsonp = window.webpackJsonp || []).push([
  [0],
  {
    114: function(e, a, t) {
      e.exports = {
        ToolWrapper: "ToolWrapper_ToolWrapper__3Zfq-",
        ToolWrapperBorder: "ToolWrapper_ToolWrapperBorder__3TLZM"
      };
    },
    178: function(e, a, t) {
      e.exports = { Heading: "Heading_Heading__9hyIj" };
    },
    180: function(e, a, t) {
      e.exports = t(401);
    },
    19: function(e, a, t) {
      e.exports = {
        Input: "Input_Input__1XbR3",
        InputWrapper: "Input_InputWrapper__2V12L",
        Label: "Input_Label__1iFz6",
        FieldContainer: "Input_FieldContainer__3Sb0U",
        Field: "Input_Field__3-7zT",
        Icon: "Input_Icon__1PhCQ",
        TrailingIcon: "Input_TrailingIcon__3Htru",
        TrailingIconMask: "Input_TrailingIconMask__2jon_",
        InformationMessage: "Input_InformationMessage__3t35m",
        FieldWithIcon: "Input_FieldWithIcon__37zsb",
        FieldWithTrailingIcon: "Input_FieldWithTrailingIcon__3-kRg",
        FieldError: "Input_FieldError__161W3",
        IconError: "Input_IconError__J-B43",
        ErrorMessage: "Input_ErrorMessage__3XDBj"
      };
    },
    3: function(e, a, t) {
      e.exports = {
        BusinessLoanCalculator:
          "BusinessLoanCalculator_BusinessLoanCalculator__2fUHx",
        InputContainer: "BusinessLoanCalculator_InputContainer__Q0whn",
        ResultsContainer: "BusinessLoanCalculator_ResultsContainer__1nDmM",
        ResultsParagraph: "BusinessLoanCalculator_ResultsParagraph__3asJD",
        ResultSection: "BusinessLoanCalculator_ResultSection__2dPqT",
        ResultsFooter: "BusinessLoanCalculator_ResultsFooter__VcEGR",
        ResultBlock: "BusinessLoanCalculator_ResultBlock__1-RPu",
        ResultLabel: "BusinessLoanCalculator_ResultLabel__Vzyvn",
        ResultValue: "BusinessLoanCalculator_ResultValue__Hvk8q",
        AwaitingResultValue:
          "BusinessLoanCalculator_AwaitingResultValue__3szp8",
        ResultBlockValue: "BusinessLoanCalculator_ResultBlockValue__2Qzm9",
        ResultSectionValue: "BusinessLoanCalculator_ResultSectionValue__3WQSs"
      };
    },
    33: function(e, a, t) {
      e.exports = {
        SliderInput: "SliderInput_SliderInput__C4mv6",
        LabelValueRow: "SliderInput_LabelValueRow__1OEg0",
        Label: "SliderInput_Label__brgws",
        Value: "SliderInput_Value__39vlP",
        ValueLabel: "SliderInput_ValueLabel__1B6Nu",
        FieldContainer: "SliderInput_FieldContainer__1mNiN",
        Ticks: "SliderInput_Ticks__1f-7C",
        Tick: "SliderInput_Tick__WvKOd",
        Slider: "SliderInput_Slider__38Slr"
      };
    },
    401: function(e, a, t) {
      "use strict";
      t.r(a);
      t(181), t(195);
      var n = t(0),
        r = t.n(n),
        l = t(174),
        i = t.n(l),
        o = t(88),
        c = t(175),
        u = t(38),
        s = t(176),
        m = t(37),
        p = function(e) {
          return function(a) {
            var t = [];
            return (
              e.forEach(function(e) {
                var n = e(a);
                n && t.push(n);
              }),
              t[0]
            );
          };
        },
        d = function(e) {
          return function(a) {
            var t;
            if ("undefined" !== typeof a && "" !== a) {
              var n = Number.parseFloat(a.replace(/[%,$]{0,1}/gi, ""));
              return Number.isNaN(n) ? (t = e) : n <= 0 && (t = e), t;
            }
          };
        },
        C = function(e) {
          return function(a) {
            var t,
              n = /^\$[0-9,.,-]+$/gi.exec(a);
            if (a && a.includes("$") && "string" === typeof a && n) {
              var r = Number.parseFloat(a.replace("$", ""));
              Number.isNaN(r) && (t = e);
            } else t = e;
            return t;
          };
        },
        f = function(e) {
          return function(a) {
            var t,
              n = /^(-){0,1}[0-9.]+[\s\h]{0,1}%$/gi.exec(a),
              r = String.fromCharCode(8201);
            if (
              a &&
              a.includes("".concat(r, "%")) &&
              "string" === typeof a &&
              n
            ) {
              var l = Number.parseFloat(a.replace(/[\s\h%]/gi, ""));
              Number.isNaN(l) && (t = e);
            } else t = e;
            return t;
          };
        },
        g = function(e) {
          return function(a) {
            var t;
            return a || 0 === a || (t = e), t;
          };
        },
        v = t(13),
        E = t(18),
        h = t.n(E),
        b = t(178),
        _ = t.n(b),
        L = function(e) {
          var a = e.id,
            t = e.title;
          return r.a.createElement("div", { id: a, className: _.a.Heading }, t);
        },
        I = function(e) {
          if (Number.isNaN(e)) return "Invalid";
          var a = e.toLocaleString("en-US", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          });
          return "$".concat(a);
        },
        R = function(e) {
          var a = (100 * e).toFixed(2).toString();
          return "".concat(a, "%");
        },
        k = t(3),
        N = t.n(k),
        y = function(e) {
          var a = e.results_title,
            t = e.results,
            n = e.isLoading,
            l = e.isEditing,
            i = n || l,
            o = t && t.loanAmount ? t.loanAmount : 0,
            c = t && t.loanTerm ? t.loanTerm : 0,
            u = t && t.loanRate ? t.loanRate : 0,
            s = t && t.monthlyPayment ? t.monthlyPayment : 0,
            m = t && t.totalBorrowingCost ? t.totalBorrowingCost : 0,
            p = t && t.averageMonthlyInterest ? t.averageMonthlyInterest : 0,
            d = t && t.totalInterest ? t.totalInterest : 0;
          return r.a.createElement(
            "div",
            {
              id: "BusinessLoanCalculatorResults",
              className: N.a.ResultsContainer
            },
            r.a.createElement(L, { title: a }),
            r.a.createElement(
              "div",
              { className: N.a.ResultsParagraph },
              "To borrow",
              " ",
              r.a.createElement(
                "span",
                {
                  className: h()(
                    N.a.ResultValue,
                    Object(v.a)({}, N.a.AwaitingResultValue, i)
                  )
                },
                I(o)
              ),
              " ",
              "over a",
              " ",
              r.a.createElement(
                "span",
                {
                  className: h()(
                    N.a.ResultValue,
                    Object(v.a)({}, N.a.AwaitingResultValue, i)
                  )
                },
                c
              ),
              " ",
              "year term your monthly payment will be\xa0",
              r.a.createElement(
                "span",
                {
                  className: h()(
                    N.a.ResultValue,
                    Object(v.a)({}, N.a.AwaitingResultValue, i)
                  )
                },
                I(s)
              ),
              " ",
              "at an interest rate\xa0of\xa0",
              r.a.createElement(
                "span",
                {
                  className: h()(
                    N.a.ResultValue,
                    Object(v.a)({}, N.a.AwaitingResultValue, i)
                  )
                },
                R(u)
              ),
              "."
            ),
            r.a.createElement(
              "div",
              { className: N.a.ResultSection },
              r.a.createElement(
                "div",
                { className: N.a.ResultLabel },
                "Total borrowing cost:"
              ),
              r.a.createElement(
                "div",
                {
                  className: h()(
                    N.a.ResultValue,
                    N.a.ResultSectionValue,
                    Object(v.a)({}, N.a.AwaitingResultValue, i)
                  )
                },
                I(m)
              )
            ),
            r.a.createElement(
              "div",
              { className: N.a.ResultsFooter },
              r.a.createElement(
                "div",
                { className: N.a.ResultBlock },
                r.a.createElement(
                  "h5",
                  { className: N.a.ResultLabel },
                  "Average Monthly Interest"
                ),
                r.a.createElement(
                  "div",
                  {
                    className: h()(
                      N.a.ResultValue,
                      N.a.ResultBlockValue,
                      Object(v.a)({}, N.a.AwaitingResultValue, i)
                    )
                  },
                  I(p)
                )
              ),
              r.a.createElement(
                "div",
                { className: N.a.ResultBlock },
                r.a.createElement(
                  "h5",
                  { className: N.a.ResultLabel },
                  "Monthly payment"
                ),
                r.a.createElement(
                  "div",
                  {
                    className: h()(
                      N.a.ResultValue,
                      N.a.ResultBlockValue,
                      Object(v.a)({}, N.a.AwaitingResultValue, i)
                    )
                  },
                  I(s)
                )
              ),
              r.a.createElement(
                "div",
                { className: N.a.ResultBlock },
                r.a.createElement(
                  "h5",
                  { className: N.a.ResultLabel },
                  "Total Interest"
                ),
                r.a.createElement(
                  "div",
                  {
                    className: h()(
                      N.a.ResultValue,
                      N.a.ResultBlockValue,
                      Object(v.a)({}, N.a.AwaitingResultValue, i)
                    )
                  },
                  I(d)
                )
              ),
              r.a.createElement(
                "div",
                { className: N.a.ResultBlock },
                r.a.createElement(
                  "h5",
                  { className: N.a.ResultLabel },
                  "Number of Years"
                ),
                r.a.createElement(
                  "div",
                  {
                    className: h()(
                      N.a.ResultValue,
                      N.a.ResultBlockValue,
                      Object(v.a)({}, N.a.AwaitingResultValue, i)
                    )
                  },
                  c
                )
              )
            )
          );
        },
        w = t(114),
        A = t.n(w),
        B = function(e) {
          return r.a.createElement(
            "div",
            { className: A.a.ToolWrapper },
            r.a.createElement(
              "div",
              { className: A.a.ToolWrapperBorder },
              e.children
            )
          );
        },
        S = t(19),
        M = t.n(S),
        j = function(e) {
          return {
            value: "$" === e ? "" : e && "$".concat(e.replace("$", "")),
            offset: 0
          };
        },
        V = function(e) {
          var a,
            t = 0,
            n = String.fromCharCode(8201);
          return (
            !(e && e.length > 2) || (e.includes("%") && e.includes(n))
              ? (a =
                  e && 2 === e.length
                    ? ""
                    : e && "".concat(e.replace(/[\s\h%]/gi, "")).concat(n, "%"))
              : ((a = "".concat(e.slice(0, e.length - 2)).concat(n, "%")),
                e.includes("%") || (t = -2),
                e.includes(n) || (t = -1)),
            (a !== "".concat(n, "%") && "%" !== a && a !== "".concat(n)) ||
              (a = ""),
            { value: a, offset: t }
          );
        },
        O = function(e) {
          setTimeout(function() {
            var a = document.getElementById(e.id),
              t = a && a.selectionStart ? a.selectionStart : 0,
              n = a && a.value ? a.value.length : 0;
            if (a && t && t >= n - 1) {
              var r = n - 2;
              a.setSelectionRange(r, r);
            }
          }, 0);
        },
        T = function(e) {
          var a,
            t,
            l = Object(n.useState)(),
            i = Object(u.a)(l, 2),
            o = i[0],
            c = i[1],
            s = e.label,
            p = e.inputName,
            d = e.placeholder,
            C = e.icon,
            f = e.trailingIcon,
            g = e.trailingIconMessage,
            E = e.format,
            b = e.validator,
            _ = e.onChange,
            L = e.onFocus,
            I = Object(m.d)(p).error;
          if (E)
            switch (E) {
              case "currency":
                t = j;
                break;
              case "percent":
                t = V;
            }
          return r.a.createElement(
            "div",
            { className: M.a.InputWrapper },
            r.a.createElement(
              "label",
              { className: M.a.Input },
              r.a.createElement("h5", { className: M.a.Label }, s),
              r.a.createElement(
                "div",
                { className: M.a.FieldContainer },
                !!C &&
                  r.a.createElement(
                    "span",
                    {
                      className: h()(
                        M.a.Icon,
                        Object(v.a)({}, M.a.IconError, !!I)
                      )
                    },
                    C()
                  ),
                o &&
                  f &&
                  !I &&
                  r.a.createElement(
                    "div",
                    { className: M.a.InformationMessage },
                    o
                  ),
                r.a.createElement(m.b, {
                  id: "input-".concat(p),
                  className: h()(
                    M.a.Field,
                    ((a = {}),
                    Object(v.a)(a, M.a.FieldWithIcon, !!C),
                    Object(v.a)(a, M.a.FieldWithTrailingIcon, !!f),
                    Object(v.a)(a, M.a.FieldError, !!I),
                    a)
                  ),
                  field: p,
                  placeholder: d,
                  maskWithCursorOffset: t,
                  onChange: function(e) {
                    "percent" === E && O(e.target), o && c(""), _ && _(e);
                  },
                  onFocus: function(e) {
                    "percent" === E && O(e.target), o && c(""), L && L(e);
                  },
                  validate: b,
                  validateOnChange: !0,
                  validateOnBlur: !0,
                  autoComplete: "off",
                  inputMode: "numeric"
                }),
                I &&
                  r.a.createElement("div", { className: M.a.ErrorMessage }, I)
              )
            ),
            !!f &&
              r.a.createElement(
                "button",
                {
                  type: "button",
                  className: h()(
                    M.a.TrailingIcon,
                    Object(v.a)({}, M.a.IconError, !!I)
                  ),
                  onClick: function() {
                    c(!o && g ? g : ""),
                      window.addEventListener(
                        "click",
                        function e(a) {
                          a.target instanceof Element &&
                            a.target.id !== "trailing-icon-mask-".concat(p) &&
                            c(""),
                            window.removeEventListener("click", e, !0);
                        },
                        !0
                      );
                  }
                },
                r.a.createElement("span", {
                  id: "trailing-icon-mask-".concat(p),
                  className: M.a.TrailingIconMask
                }),
                f()
              )
          );
        },
        x = t(33),
        F = t.n(x),
        W = Object(m.c)(function(e) {
          var a = e.fieldState,
            t = e.fieldApi,
            l = Object(o.a)(e, ["fieldState", "fieldApi"]),
            i = l.label,
            c = l.min,
            u = l.max,
            s = l.ticks,
            m = l.field,
            p = a.value,
            d = t.setValue,
            C = t.setTouched,
            f = p || "",
            g = Number.parseInt(f, 10) || 0,
            v = "" === f ? "0%" : "".concat((100 * (g - c)) / (u - c), "%");
          Object(n.useEffect)(function() {
            d(f.toString());
          }, []);
          return r.a.createElement(
            "div",
            { className: F.a.SliderInput },
            r.a.createElement(
              "label",
              null,
              r.a.createElement(
                "div",
                { className: F.a.LabelValueRow },
                r.a.createElement(
                  "h5",
                  { id: "label-".concat(m), className: F.a.Label },
                  i
                ),
                r.a.createElement("input", {
                  type: "number",
                  className: F.a.Value,
                  onChange: function(e) {
                    d(e.target.value);
                  },
                  onBlur: function(e) {
                    var a = Number.parseInt(e.target.value, 10);
                    ("" === e.target.value || a < c) && (a = c),
                      a > u && (a = u),
                      d(a.toString()),
                      C(!0);
                  },
                  value: f,
                  min: c,
                  max: u,
                  step: 1,
                  "aria-label": i,
                  "aria-labelledby": "label-".concat(m),
                  title: i
                }),
                r.a.createElement(
                  "span",
                  { className: F.a.ValueLabel },
                  "\xa0years"
                )
              ),
              r.a.createElement(
                "div",
                { className: F.a.FieldContainer },
                r.a.createElement("input", {
                  className: F.a.Slider,
                  type: "range",
                  onChange: function(e) {
                    d(e.target.value);
                  },
                  onBlur: function() {
                    C(!0);
                  },
                  min: c,
                  max: u,
                  step: 1,
                  value: f,
                  style: {
                    background: "linear-gradient(to right, hsla(206, 89%, 46%, 1) ".concat(
                      v,
                      ", hsla(0, 0%, 80%, 1) 0)"
                    )
                  },
                  title: i,
                  "aria-label": i,
                  "aria-labelledby": "label-".concat(m)
                })
              )
            ),
            s &&
              r.a.createElement(
                "div",
                { className: F.a.Ticks },
                s.map(function(e, a) {
                  return r.a.createElement(
                    "div",
                    { key: a, className: F.a.Tick },
                    e
                  );
                })
              )
          );
        }),
        P = t(89),
        Q = t.n(P),
        K = function(e) {
          var a = e.label,
            t = e.type,
            n = e.inline,
            l = void 0 !== n && n,
            i = e.disabled,
            o = void 0 !== i && i,
            c = e.isLoading,
            u = void 0 !== c && c,
            s = e.onClick;
          return r.a.createElement(
            "button",
            {
              className: h()(Q.a.Button, Object(v.a)({}, Q.a.ButtonInline, l)),
              type: t,
              disabled: o,
              onClick: s
            },
            u
              ? r.a.createElement("img", {
                  className: Q.a.Spinner,
                  alt: "loading",
                  src:
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGOfPtRkwAAA0FJREFUWAm1Vz1oU1EUvqaxP6aVKG0NrVoQB0VKTS24SAeHri7iUHQRJ0FQEQRHh4oguHV0ERyclAyig2i2Qq1DKIo0odJUYisY0wabmPj0u+Ec7nu5P+8Feofcv3O+852fe++LEB22Z58uecXKBw99hxBSLdap8sTgrFSlPojz9utcKHJ7gorqfP7jpDfQfURcOfWyTQ7ek+zh/Wd8+8ubL7yR/km5faDvqG+PdKg3RuDx4jHvxMGLYrT/rIgSZsiS8eqfTbJj7I0Ebk0VmHlq32mBaBhRlI3p0bs8+/LzNY9NAyMBKKxvL7AeouFqyHti75AUg/fnx+6xEyZdKwHkvtrYYN2nyxeMUUDK0sOXWTa7/pDH6gBy6txKAIJrlSzLox6CALQ5MXSNhuLb9pKYPfnc5z2i02jWvRvpzyKTv8kk4tAC6KHElGj+rYmt+pq4nl5iZYwz+WkvER+WBiAnREEEKx/hzuQ3vHMjt8VCaV7K4gfGsDbQneI1wsKCNIQCS/YeZwEQ+fE7J6gQQRAerlbeiavjb5gcK2gGT3Iz3szYA5FKjPt2S9WcjzyDQaE3nvQJ7zTK4ldthYn4Nh0ThFsV2aqXxKvVO22p4RqAZ9+rizINpAhCg31+D2jP1WeLj1gEY1xIwbqAAEeApf8P1JSUd1Z8NaHKuca4Ed8X73emj7yHvXxcRGz72gjYFKLuma5xKvQ4PEWeu2I9Wmxd3rSCERdhD3ZjNuMRMSOJcwRw3neTxG5FMLS3rkLme8CE6AIw6WGdjjMK0YRjPAWkDKBO7wK8nmpxU97pigd2GwHdqYCi7rMMAK5muuLpTfERcAm7jGGfIqcWn84piirXABTVxwhe420gpgBHLgGGsa6RcezBGZJByBFBGAUuWq1Zlj1HAMCtt16fczU6xF695chjdQ0OqPmWFgM/8oMEay3BQmC7NQU5NTrqB0tQAeTo2wL3Cz5ebI1TYBNqAbUkYMDWQI7CjBOAtNjknQQAQEcJwDbvyRBuV2oUDZoHeycB/DOipgLTmq5HOvE1FaY5CZD3AHQVlGoQpwfpQiHaGhehSQgAPV1JbeiREhCknAcxwqTLScDmNb2kYVMTJIj5PykZmK46LyqxAAAAAElFTkSuQmCC"
                })
              : r.a.createElement(r.a.Fragment, null, a, e.children)
          );
        },
        H = function() {
          var e = Math.random()
            .toString()
            .slice(-5);
          return r.a.createElement(
            "svg",
            {
              id: "active-".concat(e),
              width: "13px",
              height: "13px",
              viewBox: "0 0 13 13",
              version: "1.1",
              xmlns: "http://www.w3.org/2000/svg"
            },
            r.a.createElement(
              "g",
              {
                stroke: "none",
                strokeWidth: "1",
                fill: "none",
                fillRule: "evenodd"
              },
              r.a.createElement(
                "g",
                { transform: "translate(1.000000, 1.000000)" },
                r.a.createElement("circle", {
                  stroke: "currentColor",
                  cx: "5.5",
                  cy: "5.5",
                  r: "5.5"
                }),
                r.a.createElement("circle", {
                  fill: "currentColor",
                  cx: "5.5",
                  cy: "5.5",
                  r: "3.5"
                })
              )
            )
          );
        },
        q = function() {
          var e = Math.random()
            .toString()
            .slice(-5);
          return r.a.createElement(
            "svg",
            {
              id: "currency-icon-".concat(e),
              width: "18px",
              height: "17px",
              viewBox: "0 0 18 17",
              version: "1.1"
            },
            r.a.createElement(
              "defs",
              null,
              r.a.createElement("path", {
                d:
                  "M12,4.27148438 C13.1132868,4.27148438 14.1621045,4.48534942 15.1464844,4.91308594 C16.1308643,5.34082245 16.9892542,5.92089478 17.7216797,6.65332031 C18.4541052,7.38574585 19.0341775,8.2441357 19.4619141,9.22851562 C19.8896506,10.2128955 20.1035156,11.2617132 20.1035156,12.375 C20.1035156,13.4882868 19.8896506,14.5371045 19.4619141,15.5214844 C19.0341775,16.5058643 18.4541052,17.3642542 17.7216797,18.0966797 C16.9892542,18.8291052 16.1308643,19.4091775 15.1464844,19.8369141 C14.1621045,20.2646506 13.1132868,20.4785156 12,20.4785156 C10.8867132,20.4785156 9.83789555,20.2646506 8.85351562,19.8369141 C7.8691357,19.4091775 7.01074585,18.8291052 6.27832031,18.0966797 C5.54589478,17.3642542 4.96582245,16.5058643 4.53808594,15.5214844 C4.11034942,14.5371045 3.89648438,13.4882868 3.89648438,12.375 C3.89648438,11.2617132 4.11034942,10.2128955 4.53808594,9.22851562 C4.96582245,8.2441357 5.54589478,7.38574585 6.27832031,6.65332031 C7.01074585,5.92089478 7.8691357,5.34082245 8.85351562,4.91308594 C9.83789555,4.48534942 10.8867132,4.27148438 12,4.27148438 Z M12,19.125 C12.9375047,19.125 13.8134725,18.9492205 14.6279297,18.5976562 C15.4423869,18.246092 16.1572235,17.7626984 16.7724609,17.1474609 C17.3876984,16.5322235 17.871092,15.8144572 18.2226562,14.9941406 C18.5742205,14.173824 18.75,13.3007859 18.75,12.375 C18.75,11.4374953 18.5742205,10.5615275 18.2226562,9.74707031 C17.871092,8.93261312 17.3876984,8.21777651 16.7724609,7.60253906 C16.1572235,6.98730161 15.4423869,6.50390801 14.6279297,6.15234375 C13.8134725,5.80077949 12.9375047,5.625 12,5.625 C11.0624953,5.625 10.1865275,5.80077949 9.37207031,6.15234375 C8.55761312,6.50390801 7.84277651,6.98730161 7.22753906,7.60253906 C6.61230161,8.21777651 6.12890801,8.93261312 5.77734375,9.74707031 C5.42577949,10.5615275 5.25,11.4374953 5.25,12.375 C5.25,13.3125047 5.42577949,14.1884725 5.77734375,15.0029297 C6.12890801,15.8173869 6.61230161,16.5322235 7.22753906,17.1474609 C7.84277651,17.7626984 8.55761312,18.246092 9.37207031,18.5976562 C10.1865275,18.9492205 11.0624953,19.125 12,19.125 Z M12.4746094,11.7246094 C13.0605498,11.7246094 13.5585917,11.9296854 13.96875,12.3398438 C14.3789083,12.7500021 14.5839844,13.2480439 14.5839844,13.8339844 C14.5839844,14.3964872 14.3906269,14.8798808 14.0039062,15.2841797 C13.6171856,15.6884786 13.1425809,15.9082029 12.5800781,15.9433594 L12.5800781,16.3828125 C12.5800781,16.5703134 12.5156256,16.728515 12.3867188,16.8574219 C12.2578119,16.9863288 12.0996103,17.0507812 11.9121094,17.0507812 C11.7246084,17.0507812 11.5634772,16.9863288 11.4287109,16.8574219 C11.2939446,16.728515 11.2265625,16.5703134 11.2265625,16.3828125 L11.2265625,15.9082031 C10.9218735,15.8613279 10.6435559,15.7529305 10.3916016,15.5830078 C10.1396472,15.4130851 9.93164145,15.2050794 9.76757812,14.9589844 C9.7324217,14.9003903 9.70605478,14.8417972 9.68847656,14.7832031 C9.67089835,14.7246091 9.66210938,14.6601566 9.66210938,14.5898438 C9.66210938,14.4023428 9.72656186,14.2441413 9.85546875,14.1152344 C9.98437564,13.9863275 10.1425772,13.921875 10.3300781,13.921875 C10.4472662,13.921875 10.5556636,13.9482419 10.6552734,14.0009766 C10.7548833,14.0537112 10.8398434,14.1269527 10.9101562,14.2207031 L10.9101562,14.2382812 C10.9804691,14.3437505 11.0742182,14.4287106 11.1914062,14.4931641 C11.3085943,14.5576175 11.4374993,14.5898438 11.578125,14.5898438 L12.4746094,14.5898438 C12.6855479,14.5898438 12.8642571,14.5166023 13.0107422,14.3701172 C13.1572273,14.2236321 13.2304688,14.0449229 13.2304688,13.8339844 C13.2304688,13.6230458 13.1572273,13.441407 13.0107422,13.2890625 C12.8642571,13.136718 12.6855479,13.0605469 12.4746094,13.0605469 L11.5429688,13.0605469 C10.9570283,13.0605469 10.4589864,12.8554708 10.0488281,12.4453125 C9.63866982,12.0351542 9.43359375,11.5371123 9.43359375,10.9511719 C9.43359375,10.4238255 9.60644358,9.96386915 9.95214844,9.57128906 C10.2978533,9.17870897 10.7226537,8.94726598 11.2265625,8.87695312 L11.2265625,8.40234375 C11.2265625,8.21484281 11.2939446,8.05664127 11.4287109,7.92773438 C11.5634772,7.79882748 11.7246084,7.734375 11.9121094,7.734375 C12.0996103,7.734375 12.2578119,7.79882748 12.3867188,7.92773438 C12.5156256,8.05664127 12.5800781,8.21484281 12.5800781,8.40234375 L12.5800781,8.859375 C12.9433612,8.88281262 13.2744126,8.98828031 13.5732422,9.17578125 C13.8720718,9.36328219 14.1093741,9.60351416 14.2851562,9.89648438 C14.3203127,9.94335961 14.3466796,9.99609346 14.3642578,10.0546875 C14.381836,10.1132815 14.390625,10.1718747 14.390625,10.2304688 C14.390625,10.4179697 14.3232429,10.5791009 14.1884766,10.7138672 C14.0537103,10.8486335 13.8925791,10.9160156 13.7050781,10.9160156 C13.58789,10.9160156 13.476563,10.886719 13.3710938,10.828125 C13.2656245,10.769531 13.183594,10.6875005 13.125,10.5820312 C13.0546871,10.4648432 12.9609381,10.3710941 12.84375,10.3007812 C12.7265619,10.2304684 12.5917976,10.1953125 12.4394531,10.1953125 L11.5429688,10.1953125 C11.3320302,10.1953125 11.1503914,10.268554 10.9980469,10.4150391 C10.8457024,10.5615242 10.7695312,10.7402333 10.7695312,10.9511719 C10.7695312,11.1621104 10.8457024,11.3437492 10.9980469,11.4960938 C11.1503914,11.6484383 11.3320302,11.7246094 11.5429688,11.7246094 L12.4746094,11.7246094 Z",
                id: "currency-".concat(e)
              })
            ),
            r.a.createElement(
              "g",
              {
                stroke: "none",
                strokeWidth: "1",
                fill: "none",
                fillRule: "evenodd"
              },
              r.a.createElement(
                "g",
                { transform: "translate(-3.000000, -4.000000)" },
                r.a.createElement(
                  "mask",
                  { fill: "white" },
                  r.a.createElement("use", { href: "#currency-".concat(e) })
                ),
                r.a.createElement("use", {
                  fill: "currentColor",
                  href: "#currency-".concat(e)
                })
              )
            )
          );
        },
        z = function() {
          var e = Math.random()
            .toString()
            .slice(-5);
          return r.a.createElement(
            "svg",
            {
              id: "percent-icon-".concat(e),
              width: "17px",
              height: "15px",
              viewBox: "0 0 17 15",
              version: "1.1"
            },
            r.a.createElement(
              "defs",
              null,
              r.a.createElement("path", {
                d:
                  "M15.5684843,5 C16.4285439,5 17.1371,5.66292027 17.2093024,6.50496088 L17.2153609,6.64687655 L17.2153609,7.37479598 C17.2153609,7.71569943 16.9386856,7.99237469 16.5977822,7.99237469 C16.2874956,7.99237469 16.0309708,7.76371745 15.9868943,7.4660176 L15.9802034,7.37479598 L15.9802034,6.64687655 C15.9802034,6.44837646 15.8386686,6.28202906 15.6513082,6.24354378 L15.5684843,6.23515741 L7.33945392,6.23515741 C7.14095383,6.23515741 6.97460643,6.3766923 6.93612115,6.56405265 L6.92773478,6.64687655 L6.92773478,15.7298124 L11.1202708,15.7298124 C12.0054669,15.7298124 12.7255637,16.4659662 12.7255637,17.3709249 C12.7255637,18.0552021 13.2529759,18.6118464 13.9010218,18.6118464 L13.9674115,18.6156548 L14.0319485,18.6262566 L14.9509056,18.6262566 C15.4804313,18.6262566 15.9181418,18.2242064 15.9741498,17.7089893 L15.9802034,17.5969587 L15.9802034,16.9649698 C15.9802034,16.6240664 16.256467,16.3473911 16.5977822,16.3473911 C16.9076944,16.3473911 17.1645255,16.5760484 17.2086608,16.8737482 L17.2153609,16.9649698 L17.2153609,17.5969587 C17.2153609,18.7932773 16.2821508,19.7763339 15.105681,19.8561784 L14.9509056,19.861414 L7.95703263,19.861414 L7.89064291,19.8576056 L7.82610594,19.8470038 L6.41061555,19.8470038 C5.1343521,19.8470038 4.08689118,18.8236532 4.00513633,17.5335256 L4,17.3709249 L4,17.2004732 C4,16.437485 4.57258877,15.8079792 5.30243029,15.7365598 L5.44101698,15.7298124 L5.69257737,15.7298124 L5.69257737,6.64687655 C5.69257737,5.78642689 6.35549765,5.07821979 7.19753825,5.00605519 L7.33945392,5 L15.5684843,5 Z M11.1202708,16.9649698 L5.44101698,16.9649698 C5.34632158,16.9649698 5.26649381,17.0384503 5.24251498,17.1379967 L5.23515741,17.2004732 L5.23515741,17.3709249 C5.23515741,18.0124348 5.6987033,18.5417673 6.29061992,18.605428 L6.41061555,18.6118464 L11.8152527,18.6118464 C11.6085697,18.2466515 11.4904063,17.8229925 11.4904063,17.3709249 C11.4904063,17.1469497 11.3244834,16.9649698 11.1202708,16.9649698 Z M18.5133054,12.480772 C19.4487313,12.480772 20.21,13.241629 20.21,14.1774666 C20.21,15.1124808 19.4487313,15.8737494 18.5133054,15.8737494 C17.5778796,15.8737494 16.8166109,15.1124808 16.8166109,14.1774666 C16.8166109,13.241629 17.5778796,12.480772 18.5133054,12.480772 Z M19.7608556,8.57384562 C19.9753155,8.78830555 19.9991444,9.12085262 19.8323422,9.36155413 L19.7608556,9.44710191 L14.0165502,15.1914073 C13.8959165,15.312041 13.7378163,15.372152 13.5801279,15.372152 C13.4220278,15.372152 13.2639276,15.312041 13.1432939,15.1914073 C12.928834,14.9769474 12.9050051,14.6444003 13.0718073,14.4036988 L13.1432939,14.318151 L18.8875993,8.57384562 C19.1288667,8.33257821 19.5195882,8.33257821 19.7608556,8.57384562 Z M18.5,13.7159294 C18.2243533,13.7159294 18,13.9403828 18,14.2161526 C18,14.4914761 18.2243533,14.7159294 18.5,14.7159294 C18.7756467,14.7159294 19,14.4914761 19,14.2161526 C19,13.9403828 18.7756467,13.7159294 18.5,13.7159294 Z M14.0239611,7.99122187 C14.959387,7.99122187 15.720244,8.75249056 15.720244,9.68791644 C15.720244,10.6229306 14.959387,11.3841993 14.0239611,11.3841993 C13.0885353,11.3841993 12.3272666,10.6229306 12.3272666,9.68791644 C12.3272666,8.75249056 13.0885353,7.99122187 14.0239611,7.99122187 Z M14.0002231,9.22637928 C13.7244534,9.22637928 13.5,9.45083265 13.5,9.7266024 C13.5,10.0019259 13.7244534,10.2263793 14.0002231,10.2263793 C14.2755466,10.2263793 14.5,10.0019259 14.5,9.7266024 C14.5,9.45083265 14.2755466,9.22637928 14.0002231,9.22637928 Z",
                id: "percent-".concat(e)
              })
            ),
            r.a.createElement(
              "g",
              {
                stroke: "none",
                strokeWidth: "1",
                fill: "none",
                fillRule: "evenodd"
              },
              r.a.createElement(
                "g",
                { transform: "translate(-4.000000, -5.000000)" },
                r.a.createElement(
                  "mask",
                  { fill: "white" },
                  r.a.createElement("use", { href: "#percent" })
                ),
                r.a.createElement("use", {
                  fill: "currentColor",
                  href: "#percent-".concat(e)
                })
              )
            )
          );
        },
        Z = function() {
          var e = Math.random()
            .toString()
            .slice(-5);
          return r.a.createElement(
            "svg",
            {
              id: "refresh-icon-".concat(e),
              width: "16px",
              height: "16px",
              viewBox: "0 0 16 16",
              version: "1.1",
              xmlns: "http://www.w3.org/2000/svg"
            },
            r.a.createElement(
              "defs",
              null,
              r.a.createElement("path", {
                d:
                  "M14.6666667,9.33333333 C14.9465379,9.33333333 15.1861294,9.50579141 15.2849687,9.75023518 L15.2882892,9.7586198 C15.3185678,9.83667296 15.3335354,9.91915447 15.3333354,10.001518 L15.3333354,14 C15.3333354,14.3681898 15.0348565,14.6666667 14.6666667,14.6666667 C14.2984768,14.6666667 14,14.3681898 14,14 L14,11.5410786 L12.044856,13.3779537 C10.426664,14.9969563 8.09100876,15.6725468 5.85840168,15.1673914 C3.62579461,14.6622361 1.80847199,13.0469822 1.04485597,10.8890657 C0.922029224,10.5419673 1.10383738,10.1610178 1.45093584,10.0381911 C1.79803431,9.91536438 2.17898396,10.0971725 2.3018107,10.4442708 C2.91270352,12.1706041 4.36656161,13.4628071 6.15264727,13.8669314 C7.93873294,14.2710557 9.80725713,13.7305834 11.1168156,12.4208323 L12.9836347,10.6666667 L10.6666667,10.6666667 C10.2984768,10.6666667 10,10.3681898 10,10 C10,9.63181017 10.2984768,9.33333333 10.6666667,9.33333333 L14.6666667,9.33333333 Z M9.47493367,0.831157578 C11.7075407,1.33631295 13.5248634,2.95156678 14.2884794,5.10948335 C14.4113061,5.4565817 14.229498,5.83753122 13.8823995,5.96035792 C13.535301,6.08318462 13.1543514,5.90137653 13.0315247,5.55427818 C12.4206318,3.82794493 10.9667737,2.53574186 9.18068808,2.13161756 C7.39460242,1.72749327 5.52607823,2.26796565 4.21651974,3.57771673 L2.34970067,5.33188233 L4.66666869,5.33188233 C5.03485852,5.33188233 5.33333535,5.63035917 5.33333535,5.998549 C5.33333535,6.36673883 5.03485852,6.66521567 4.66666869,6.66521567 L0.666668685,6.66521567 C0.386797458,6.66521567 0.147205974,6.49275759 0.0483666255,6.24831382 L0.0450461346,6.2399292 C0.0147675214,6.16187604 -0.000200036853,6.07939453 -1.7408297e-13,5.99703101 L-1.7408297e-13,1.998549 C-1.7408297e-13,1.63035917 0.298478852,1.33188233 0.666668685,1.33188233 C1.03485852,1.33188233 1.33333535,1.63035917 1.33333535,1.998549 L1.33333535,4.45747036 L3.28847939,2.6205953 C4.90667135,1.00159268 7.24232659,0.326002207 9.47493367,0.831157578 Z",
                id: "refresh-path-".concat(e)
              })
            ),
            r.a.createElement(
              "g",
              {
                stroke: "none",
                strokeWidth: "1",
                fill: "none",
                fillRule: "evenodd"
              },
              r.a.createElement(
                "g",
                { transform: "translate(-100.000000, -18.000000)" },
                r.a.createElement(
                  "g",
                  { transform: "translate(100.000000, 0.000000)" },
                  r.a.createElement(
                    "g",
                    { transform: "translate(0.000000, 18.000000)" },
                    r.a.createElement(
                      "mask",
                      { fill: "white" },
                      r.a.createElement("use", {
                        href: "#refresh-path-".concat(e)
                      })
                    ),
                    r.a.createElement("use", {
                      id: "Icon-Colour",
                      fill: "currentColor",
                      transform:
                        "translate(7.666668, 7.999275) scale(-1, 1) translate(-7.666668, -7.999275) ",
                      href: "#refresh-path-".concat(e)
                    })
                  )
                )
              )
            )
          );
        },
        D = function(e) {
          return e ? Number.parseFloat(e.replace(/[$,]/gi, "")) : NaN;
        },
        G = function(e) {
          return e ? Number.parseFloat(e.replace(/[\s\h%]/gi, "")) / 100 : 0;
        },
        U = function(e) {
          var a = D(e.loanAmount),
            t = G(e.loanRate),
            n = D(e.loanTerm),
            r = 12 * n,
            l = t / 12,
            i = (l * a) / (1 - Math.pow(1 + l, -r)),
            o = r * i - (a - 0);
          return {
            loanAmount: a,
            loanRate: t,
            loanTerm: n,
            monthlyPayment: i,
            totalBorrowingCost: o,
            averageMonthlyInterest: o / r,
            totalInterest: o
          };
        },
        Y = p([
          g("Loan amount required"),
          C("Please enter a valid currency input"),
          d("Please enter a value greater than zero")
        ]),
        J = p([
          g("Loan amount required"),
          f("Please enter a valid percent input"),
          d("Please enter a value greater than zero")
        ]),
        $ = function(e) {
          var a = Object(n.useState)(),
            t = Object(u.a)(a, 2),
            l = t[0],
            i = t[1],
            o = Object(n.useState)(),
            p = Object(u.a)(o, 2),
            d = p[0],
            C = p[1],
            f = Object(n.useState)(!1),
            g = Object(u.a)(f, 2),
            v = g[0],
            E = g[1],
            h = e.title,
            b = e.input_amount_label,
            _ = e.input_rate_label,
            I = e.input_term_label;
          return r.a.createElement(
            B,
            null,
            r.a.createElement(
              m.a,
              {
                className: N.a.BusinessLoanCalculator,
                onSubmit: function(e) {
                  E(!0);
                  var a = U(e);
                  C(Object(c.a)({}, e)),
                    i(a),
                    E(!1),
                    (function(e) {
                      var a = document.getElementById(e),
                        t = window.matchMedia(
                          "(prefers-reduced-motion: reduce)"
                        ),
                        n = window.matchMedia(
                          "only screen and (min-width: 800px)"
                        );
                      if (!((t && t.matches) || (n && n.matches)) && a) {
                        var r = a.scrollHeight;
                        a.scrollIntoView
                          ? a.scrollIntoView({
                              behavior: "smooth",
                              block: "end",
                              inline: "nearest"
                            })
                          : window.scroll(0, r);
                      }
                    })("BusinessLoanCalculatorResults");
                }
              },
              function(a) {
                var t = a.formState,
                  n = a.formApi,
                  o = !Object(s.isEqual)(t.values, d),
                  c = t.invalid;
                return r.a.createElement(
                  r.a.Fragment,
                  null,
                  r.a.createElement(
                    "div",
                    { className: N.a.InputContainer },
                    r.a.createElement(L, {
                      id: "BusinessLoanCalculatorTitle",
                      title: h
                    }),
                    r.a.createElement(T, {
                      label: b,
                      inputName: "loanAmount",
                      placeholder: "$0.00",
                      icon: q,
                      format: "currency",
                      validator: Y
                    }),
                    r.a.createElement(T, {
                      label: _,
                      inputName: "loanRate",
                      placeholder: "0.00%",
                      icon: z,
                      format: "percent",
                      validator: J
                    }),
                    r.a.createElement(W, {
                      label: I,
                      field: "loanTerm",
                      min: 1,
                      max: 30,
                      initialValue: "10",
                      ticks: ["1yr", "10yr", "20yr", "30yr"]
                    }),
                    r.a.createElement(K, {
                      type: "submit",
                      label: "Calculate",
                      disabled: v || c,
                      isLoading: v
                    }),
                    r.a.createElement(
                      K,
                      {
                        type: "button",
                        onClick: function() {
                          n.reset(), i(null);
                        },
                        inline: !0
                      },
                      r.a.createElement(Z, null),
                      " \xa0Refresh"
                    )
                  ),
                  r.a.createElement(
                    y,
                    Object.assign({ results: l, isLoading: v, isEditing: o }, e)
                  )
                );
              }
            )
          );
        },
        X = t(179),
        ee = {
          "cost-margin": function(e) {
            var a = e.cost,
              t = e.margin,
              n = {};
            return (
              (n.markup = 1 / (1 - t) - 1),
              (n.revenue = a * (1 + n.markup)),
              (n.profit = n.revenue - a),
              n
            );
          },
          "cost-markup": function(e) {
            var a = e.cost,
              t = e.markup,
              n = {};
            return (
              (n.revenue = a * (1 + t)),
              (n.profit = n.revenue - a),
              (n.margin = n.profit / n.revenue),
              n
            );
          },
          "cost-revenue": function(e) {
            var a = e.cost,
              t = e.revenue,
              n = {};
            return (
              (n.profit = t - a),
              (n.margin = n.profit / t),
              (n.markup = n.profit / a),
              n
            );
          },
          "cost-profit": function(e) {
            var a = e.cost,
              t = e.profit,
              n = {};
            return (
              (n.revenue = t + a),
              (n.margin = t / n.revenue),
              (n.markup = t / a),
              n
            );
          },
          "markup-revenue": function(e) {
            var a = e.markup,
              t = e.revenue,
              n = {};
            return (
              (n.margin = 1 - 1 / (a + 1)),
              (n.cost = t / (1 + a)),
              (n.profit = t - n.cost),
              n
            );
          },
          "markup-profit": function(e) {
            var a = e.markup,
              t = e.profit,
              n = {};
            return (
              (n.margin = 1 - 1 / (a + 1)),
              (n.cost = t / a),
              (n.revenue = t + n.cost),
              n
            );
          },
          "margin-revenue": function(e) {
            var a = e.margin,
              t = e.revenue,
              n = {};
            return (
              (n.markup = 1 / (1 - a) - 1),
              (n.cost = t / (1 + n.markup)),
              (n.profit = t - n.cost),
              n
            );
          },
          "margin-profit": function(e) {
            var a = e.margin,
              t = e.profit,
              n = {};
            return (
              (n.markup = 1 / (1 - a) - 1),
              (n.cost = t / n.markup),
              (n.revenue = t + n.cost),
              n
            );
          },
          "profit-revenue": function(e) {
            var a = e.profit,
              t = e.revenue,
              n = {};
            return (
              (n.cost = t - a), (n.margin = a / t), (n.markup = a / n.cost), n
            );
          }
        },
        ae = t(90),
        te = t.n(ae),
        ne = ["cost", "profit", "revenue"],
        re = ["cost", "profit", "revenue"],
        le = ["margin", "markup"],
        ie = C("Invalid entry"),
        oe = f("Invalid entry"),
        ce = {
          BusinessLoanCalculator: $,
          MarkupCalculator: function(e) {
            var a = Object(n.useState)([]),
              t = Object(u.a)(a, 2),
              l = t[0],
              i = t[1],
              o = e.title,
              c = e.subtitle,
              s = "Input value used to calculate results.";
            return r.a.createElement(
              B,
              null,
              r.a.createElement(
                m.a,
                { className: te.a.MarkupCalculator },
                function(e) {
                  var a = e.formApi,
                    t = e.formState,
                    n = function(e) {
                      var n = e && e.target.name,
                        r = t.values,
                        o = t.errors,
                        c = (function(e, a) {
                          var t = Object(X.a)(a);
                          return (
                            Array.isArray(t) && 0 === t.length
                              ? t.push(e)
                              : Array.isArray(t) && 1 === t.length
                              ? t[0] !== e && t.push(e)
                              : Array.isArray(t) &&
                                2 === t.length &&
                                e !== t[1] &&
                                (("margin" === e && "markup" === t[1]) ||
                                ("markup" === e && "margin" === t[1])
                                  ? e !== t[0]
                                    ? (t.pop(), t.push(e))
                                    : (t.shift(), t.pop(), t.push(e))
                                  : (t.shift(), t.push(e))),
                            i(t),
                            t
                          );
                        })(n, l),
                        s = Object(u.a)(c, 2),
                        m = s[0],
                        p = s[1];
                      if (p && m) {
                        var d = [p, m].sort().join("-");
                        if (
                          ee.hasOwnProperty(d) &&
                          r &&
                          r.hasOwnProperty(p) &&
                          r.hasOwnProperty(m) &&
                          !o[p] &&
                          !o[m]
                        ) {
                          var C,
                            f = (function(e) {
                              var a = {};
                              return (
                                Object.keys(e).map(function(t) {
                                  if (ne.includes(t)) {
                                    var n = e[t];
                                    n && (a[t] = I(n));
                                  } else if (le.includes(t)) {
                                    var r = e[t];
                                    r && (a[t] = R(r));
                                  }
                                }),
                                a
                              );
                            })(
                              (0, ee[d])(
                                (function(e) {
                                  var a = {};
                                  return (
                                    Object.keys(e).map(function(t) {
                                      re.includes(t) || ne.includes(t)
                                        ? (a[t] = D(e[t]))
                                        : le.includes(t) && (a[t] = G(e[t]));
                                    }),
                                    a
                                  );
                                })(
                                  ((C = {}),
                                  Object(v.a)(C, p, r[p]),
                                  Object(v.a)(C, m, r[m]),
                                  C)
                                )
                              )
                            );
                          a.setValues(f);
                        }
                      }
                    };
                  return r.a.createElement(
                    "div",
                    { className: te.a.InputContainer },
                    r.a.createElement(L, {
                      id: "MarkupCalculatorTitle",
                      title: o
                    }),
                    c &&
                      r.a.createElement("div", { className: te.a.Subtitle }, c),
                    r.a.createElement(T, {
                      label: "Cost",
                      inputName: "cost",
                      placeholder: "$0.00",
                      icon: q,
                      trailingIcon: l && l.includes("cost") ? H : void 0,
                      trailingIconMessage: s,
                      format: "currency",
                      onChange: n,
                      validator: ie
                    }),
                    r.a.createElement(T, {
                      label: "Markup",
                      inputName: "markup",
                      placeholder: "0%",
                      icon: z,
                      trailingIcon: l && l.includes("markup") ? H : void 0,
                      trailingIconMessage: s,
                      format: "percent",
                      onChange: n,
                      validator: oe
                    }),
                    r.a.createElement(T, {
                      label: "Margin",
                      inputName: "margin",
                      placeholder: "0%",
                      icon: z,
                      trailingIcon: l && l.includes("margin") ? H : void 0,
                      trailingIconMessage: s,
                      format: "percent",
                      onChange: n,
                      validator: oe
                    }),
                    r.a.createElement(T, {
                      label: "Revenue",
                      inputName: "revenue",
                      placeholder: "$0.00",
                      icon: q,
                      trailingIcon: l && l.includes("revenue") ? H : void 0,
                      trailingIconMessage: s,
                      format: "currency",
                      onChange: n,
                      validator: ie
                    }),
                    r.a.createElement(T, {
                      label: "Profit",
                      inputName: "profit",
                      placeholder: "$0.00",
                      icon: q,
                      trailingIcon: l && l.includes("profit") ? H : void 0,
                      trailingIconMessage: s,
                      format: "currency",
                      onChange: n,
                      validator: ie
                    }),
                    r.a.createElement(
                      K,
                      {
                        type: "button",
                        onClick: function() {
                          a.reset(), i([]);
                        },
                        inline: !0
                      },
                      r.a.createElement(Z, null),
                      " \xa0Refresh"
                    )
                  );
                }
              )
            );
          }
        },
        ue = function(e) {
          var a = {};
          return (
            Array.prototype.slice.call(e.attributes).forEach(function(e) {
              if (
                "name" in e &&
                "string" === typeof e.name &&
                0 === e.name.indexOf("data-")
              ) {
                var t = "data-".length;
                a[e.name.substr(t)] = e.value;
              }
            }),
            a
          );
        },
        se = function(e) {
          var a = ue(e);
          if (!("tool" in a)) throw new Error("Tool attribute is required");
          var t = a.tool,
            n = Object(o.a)(a, ["tool"]);
          if (!t || !(t in ce))
            throw new Error("Tool ".concat(t, " does not exist."));
          var l = ce[t];
          return r.a.createElement(l, n);
        };
      window.addEventListener("DOMContentLoaded", function() {
        var e = document.getElementsByClassName("tool");
        Array.prototype.forEach.call(e, function(e) {
          i.a.hydrate(r.a.createElement(n.StrictMode, null, se(e)), e);
        });
      });
    },
    89: function(e, a, t) {
      e.exports = {
        Button: "Button_Button__a_dBg",
        ButtonInline: "Button_ButtonInline__l6SO-",
        Spinner: "Button_Spinner__3yyLX",
        spin: "Button_spin__PcDKp"
      };
    },
    90: function(e, a, t) {
      e.exports = {
        MarkupCalculator: "MarkupCalculator_MarkupCalculator__1P1pE",
        InputContainer: "MarkupCalculator_InputContainer__30k8j",
        Subtitle: "MarkupCalculator_Subtitle__3WRDc"
      };
    }
  },
  [[180, 1, 2]]
]);
//# sourceMappingURL=main.b6835608.chunk.js.map
