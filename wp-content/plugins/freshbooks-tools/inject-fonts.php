<?php
function inject_fonts($plugin_name, $stylesheet_name) {
  $dir = plugins_url();
  $plugin_dir = $dir . "/" . $plugin_name;
  $font_css = "
    @font-face {
      font-family: Franklin-Gothic-URW;
      font-style: light;
      font-weight: 300;
      src: local('Franklin-Gothic-URW-Light'),
        url({$plugin_dir}/fonts/FranklinGothicURW-Lig.woff)
          format('woff');
    }
    @font-face {
      font-family: Franklin-Gothic-URW;
      font-style: book;
      font-weight: 400;
      src: local('Franklin-Gothic-URW-Book'),
        url({$plugin_dir}/fonts/FranklinGothicURW-Boo.woff)
          format('woff');
    }
    @font-face {
      font-family: Franklin-Gothic-URW;
      font-style: medium;
      font-weight: 500;
      src: local('Franklin-Gothic-URW-Medium'),
        url({$plugin_dir}/fonts/FranklinGothicURW-Med.woff)
          format('woff');
    }";

  wp_add_inline_style( $stylesheet_name, $font_css );
};