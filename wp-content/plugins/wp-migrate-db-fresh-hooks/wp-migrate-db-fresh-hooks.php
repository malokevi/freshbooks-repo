<?php
/**
 * Plugin Name:     FreshBooks WP Migrate DB Hooks
 * Plugin URI:      https://github.com/freshbooks/wp-migrate-db-fresh-hooks/
 * Description:     A plugin to run commands and setup environments before/after migration using WP Migrate DB.
 * Author:          FreshBooks
 * Text Domain:     wp-migrate-db-fresh-hooks
 * Domain Path:     /lang
 * Version:         1.3.5
 *
 * @package FreshHooks
 */

namespace FreshHooks;

if (!defined('ABSPATH')) {
    exit;
}

define('FRESH_HOOKS_PLUGIN_FILE', __FILE__);

require 'inc/autoload.php';
require 'inc/plugin-update-checker/plugin-update-checker.php';

FreshHooks::getInstance()->addAllHandlers();
