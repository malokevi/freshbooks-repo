<?php
/**
 * Module class providing default functionality.
 *
 * @package FreshHooks
 */

namespace FreshHooks;

use FreshHooks\ActionHandlers;

abstract class Module
{
    public function addHandlers() {
        foreach (ActionHandlers::getAll() as $actionHandler) {
            if (isset($actionHandler['name']) && isset($actionHandler['handler'])) {
                $priority = isset($actionHandler['priority']) ? $actionHandler['priority'] : 10;
                $numParams = isset($actionHandler['params']) ? $actionHandler['params'] : 1;

                if (method_exists($this, $actionHandler['handler'])) {
                    add_filter($actionHandler['name'], [ $this, $actionHandler['handler'] ], $priority, $numParams);
                }
            }
        }
    }

    protected function isSource($migrationType) {
        return !$this->isTarget($migrationType);
    }

    protected function isTarget($migrationType) {
        return isset($_POST['action']) && (
            ($_POST['action'] === 'wpmdb_remote_finalize_migration' && $migrationType === 'push') ||
            ($_POST['action'] === 'wpmdb_finalize_migration' && $migrationType === 'pull')
        );
    }
}
