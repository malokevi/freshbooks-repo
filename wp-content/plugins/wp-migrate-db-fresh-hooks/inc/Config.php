<?php
/**
 * Config trait providing default functionality.
 *
 * @package FreshHooks
 */

namespace FreshHooks;

trait Config
{
    public static function get($value, $key = null) {
        if (is_null($key)) {
            $key = self::$defaultKey;
        }

        $index = array_search($value, array_column(self::$config, $key));

        if ($index !== false) {
            return self::$config[$index];
        }

        return [];
    }

    public static function getAll() {
        return self::$config;
    }
}
