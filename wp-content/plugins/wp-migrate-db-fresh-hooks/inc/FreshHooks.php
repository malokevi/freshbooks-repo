<?php
/**
 * Core class for managing our WP Migrate DB hooks.
 *
 * @package FreshHooks
 */

namespace FreshHooks;

use FreshHooks\Modules;

class FreshHooks
{
    private static $instance = null;

    public static $modules = [];

    public static $updater = null;

    private function __construct() {
        self::$modules = [
            new Modules\CacheClearer(),
            new Modules\OptionsManager(),
        ];

        self::initPluginUpdater();
    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function addAllHandlers() {
        foreach (self::$modules as $module) {
            if (method_exists($module, 'addHandlers')) {
                $module->addHandlers();
            }
        }
    }

    /**
     * Throw error on object clone.
     *
     * The whole idea of the singleton design pattern is that there is a single
     * object, therefore we don't want the object to be cloned.
     */
    public function __clone() {
        _doing_it_wrong(__FUNCTION__, __('Forbidden', 'wp-migrate-db-fresh-hooks'), '1.1.0');
    }

    /**
     * Disable unserializing of the class.
     */
    public function __wakeup() {
        _doing_it_wrong(__FUNCTION__, __('Forbidden', 'wp-migrate-db-fresh-hooks'), '1.1.0');
    }

    /**
     * Initialize the plugin updater
     */
    public static function initPluginUpdater() {
        if (self::$updater === null) {
            self::$updater = \Puc_v4_Factory::buildUpdateChecker('https://github.com/freshbooks/wp-migrate-db-fresh-hooks/', FRESH_HOOKS_PLUGIN_FILE, 'wp-migrate-db-fresh-hooks', 12);
            self::$updater->setAuthentication('b715db84d7c1c0572839a7fbd11402789a595781');
            self::$updater->getVcsApi()->enableReleaseAssets();
        }
    }
}
