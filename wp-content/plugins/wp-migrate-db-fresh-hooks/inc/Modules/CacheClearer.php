<?php
/**
 * Cache clearing functions for the FreshBooks public website.
 *
 * @package FreshHooks
 */

namespace FreshHooks\Modules;

use FreshHooks\Module;
use FreshHooks\Environments;

class CacheClearer extends Module
{
    private $fastlyApiToken;

    private $fastlyApiUrl;

    private static $kinstaClearPath = '/kinsta-clear-cache-all/';

    public function __construct() {
        $fastlyOptions = get_option('fastly-settings-general', []);

        if (isset($fastlyOptions['fastly_api_key'])) {
            $this->fastlyApiToken = $fastlyOptions['fastly_api_key'];
        }
        if (isset($fastlyOptions['fastly_api_hostname'])) {
            $this->fastlyApiUrl = untrailingslashit($fastlyOptions['fastly_api_hostname']);
        }
    }

    /**
     * Run cache clear commands based on target action result and migration type.
     */
    public function migrationComplete($migrationType, $connectionUrl) {
        if ($this->isTarget($migrationType)) {
            $targetEnv = Environments::getCurrent();

            if (!empty($targetEnv)) {
                $this->clearKinstaCache($targetEnv);
                $this->clearFastlyCache($targetEnv);
            }
        }
    }

    /**
     * Run Kinsta cache clear if applicable to the environment.
     */
    private function clearKinstaCache($env) {
        if (!empty($env) && $env['kinsta'] && isset($env['url'])) {
            $curlHandle = curl_init();
            curl_setopt_array($curlHandle, [
                CURLOPT_URL            => untrailingslashit($env['url']) . self::$kinstaClearPath,
                CURLOPT_RETURNTRANSFER => true,
            ]);
            $result = curl_exec($curlHandle);
            curl_close($curlHandle);

            error_log("${env['name']}: ${result}");
        }
    }

    /**
     * Run Fastly cache clear if applicable to the environment.
     */
    private function clearFastlyCache($env) {
        if (isset($this->fastlyApiToken) && isset($this->fastlyApiUrl) && !empty($env) && $env['fastly'] && isset($env['service'])) {
            $curlHandle = curl_init();
            curl_setopt_array($curlHandle, [
                CURLOPT_URL            => "{$this->fastlyApiUrl}/service/{$env['service']}/purge/public-website",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST           => true,
                CURLOPT_HTTPHEADER     => [
                    'Host: api.fastly.com',
                    'Fastly-Key: ' . $this->fastlyApiToken,
                    'Accept: application/json',
                    'Fastly-Soft-Purge: 1',
                ],
            ]);
            $result = curl_exec($curlHandle);
            curl_close($curlHandle);

            error_log("${env['name']}: ${result}");
        }
    }
}
