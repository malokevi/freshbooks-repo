<?php
/**
 * Preserve and/or set options when running a WP Migrate push or pull.
 *
 * @package FreshHooks
 */

namespace FreshHooks\Modules;

use FreshHooks\Module;

class OptionsManager extends Module
{
    public function preservedOptions($options) {
        // SAML preserved options
        $options[] = 'mo_saml_relay_state';
        $options[] = 'saml_issuer';
        $options[] = 'saml_login_url';
        $options[] = 'saml_metadata_url_for_sync';
        $options[] = 'site_ck_l';
        $options[] = 'sml_lk';

        // Fastly preserved options
        $options[] = 'fastly-settings-general';

        // ACF preserved options
        $options[] = 'acf_pro_license';

        return $options;
    }
}
