<?php
/**
 * WP Migrate DB actions and handlers.
 *
 * @package FreshHooks
 */

namespace FreshHooks;

use FreshHooks\Config;

class ActionHandlers
{
    use Config;

    /**
     * Add handlers for WP Migrate DB actions/filters
     *
     * For possible actions see: https://github.com/deliciousbrains/wp-migrate-db-pro-tweaks
     */
    protected static $config = [
        [
            'name'     => 'wpmdb_migration_complete',
            'handler'  => 'migrationComplete',
            'priority' => 8,
            'params'   => 2,
        ],
        [
            'name'     => 'wpmdb_preserved_options',
            'handler'  => 'preservedOptions',
            'priority' => 10,
            'params'   => 1,
        ],
    ];

    protected static $defaultKey = 'name';
}
