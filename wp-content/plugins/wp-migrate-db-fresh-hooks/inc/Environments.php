<?php
/**
 * FreshBooks public website environments.
 *
 * @package FreshHooks
 */

namespace FreshHooks;

use FreshHooks\Config;

class Environments
{
    use Config;

    protected static $config = [
        [
            'name'    => 'freshprod',
            'url'     => 'https://www.freshbooks.com',
            'service' => '7QWoGtxjp3DwsNG7SOxZuz',
            'fastly'  => true,
            'kinsta'  => true,
        ],
        [
            'name'    => 'freshstaging',
            'url'     => 'https://www.staging.freshenv.com',
            'service' => '1dCi4dVvXF4HkVBcL9bVLR',
            'fastly'  => true,
            'kinsta'  => true,
        ],
        [
            'name'    => 'freshuat',
            'url'     => 'https://www.uat.freshenv.com',
            'service' => '3p6Z8avXrpLDn7W8ZUvwSQ',
            'fastly'  => true,
            'kinsta'  => true,
        ],
        [
            'name'    => 'qatunezo.kinsta.com',
            'url'     => 'https://qatunezo.kinsta.com',
            'service' => '3p6Z8avXrpLDn7W8ZUvwSQ',
            'fastly'  => false,
            'kinsta'  => true,
        ],
        [
            'name'   => 'freshdev',
            'url'    => 'https://freshdev.kinsta.com',
            'fastly' => false,
            'kinsta' => true,
        ],
        [
            'name'   => 'localdev',
            'url'    => 'http://www.freshbooks.test:8080',
            'fastly' => false,
            'kinsta' => false,
        ],
    ];

    protected static $defaultKey = 'url';

    public static function getCurrent() {
        return self::get(home_url());
    }
}
