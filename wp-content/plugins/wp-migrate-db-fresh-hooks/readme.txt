=== FreshBooks WP Migrate DB Hooks ===
Plugin Name: FreshBooks WP Migrate DB Hooks
Plugin URI: https://github.com/freshbooks/wp-migrate-db-fresh-hooks/
Author: FreshBooks
Requires at least: 4.7
Tested up to: 5.4
Stable tag: 1.3.5

A plugin to run commands and setup environments before/after migration using WP Migrate DB.

== Installation ==

Install using your choice of the regular WordPress plugin methods.

== Changelog ==

= 1.3.5 =
* Fixed a bug with missing preserved SAML SSO plugin options

= 1.3.4 =
* Added wp_options retention for SAML SSO plugin options

= 1.3.3 =
* Fixed broken update checking by updating to latest version of plugin-update-checker

= 1.3.2 =
* Updated remote environments
* Allow empty version for publish command

= 1.3.1 =
* Added remote deployment feature to local cli

= 1.3.0 =
* Added plugin update feature

= 1.2.4 =
* Added PHP linting

= 1.2.2 =
* Add versioning tasks

= 1.2.1 =
* Preserve ACF Pro license option value

= 1.2.0 =
* Added OptionsManager module

= 1.1.0 =
* Implemented PSR-4 autoloading for classes
* Added abstract Module class for common module functionality
* Added Config trait for common config functionality
* Added Config classes for Environments and ActionHandlers
* Renamed and moved core FreshHooks class to separate file

= 1.0.0 =
* Initial version
