    

    <?php
        $counter = 0;
    ?>

    <?php $__env->startSection('content'); ?>

    <?php if(have_rows('flexible_content')): ?>

        <?php while(have_rows('flexible_content')): ?> <?php (the_row()); ?>
            <?php if(get_row_layout() === 'home_page_header'): ?>
                <?php echo $__env->make('partials/fc-content/global/hero-with-cta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'three_col_features'): ?>
                <?php echo $__env->make('partials/fc-content/home/three-col-features', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'sub_header_menu'): ?>
                <?php echo $__env->make('partials/fc-content/global/sub-header-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'main_content_centered'): ?>
                <?php echo $__env->make('partials/fc-content/global/main-content-centered', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'two_col_image_content'): ?>
                <?php echo $__env->make('partials/fc-content/global/two-col-image-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'cta_full_width'): ?>
                <?php echo $__env->make('partials/fc-content/global/cta-full-width', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'featured_notice'): ?>
                <?php echo $__env->make('partials/fc-content/global/featured-notice', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'featured_in'): ?>
                <?php echo $__env->make('partials/fc-content/global/featured-in', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'cta_tablet_and_mobile_only'): ?>
                <?php echo $__env->make('partials/fc-content/home/centered-cta-devices-only', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'three_col_content'): ?>
                <?php echo $__env->make('partials/fc-content/home/three-col-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'content_carousel'): ?>
                <?php echo $__env->make('partials/fc-content/global/content-carousel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'testimonial_columns'): ?>
                <?php echo $__env->make('partials/fc-content/global/testimonial-columns', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'cta_full_width_outline'): ?>
                <?php echo $__env->make('partials/fc-content/global/cta-full-width-outline', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'two_col_cta'): ?>
                <?php echo $__env->make('partials/fc-content/home/two-col-cta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'background_image_full_width'): ?>
                <?php echo $__env->make('partials/fc-content/home/background-image-full-width', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'two_col_addon_section'): ?>
                <?php echo $__env->make('partials/fc-content/home/two-col-addon-section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'divider'): ?>
                <?php echo $__env->make('partials/fc-content/global/divider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'two_col_support'): ?>
                <?php echo $__env->make('partials/fc-content/global/two-col-support', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'sticky_nav'): ?>
                <?php echo $__env->make('partials/fc-content/home/sticky-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'cta_download_templates'): ?>
                <?php echo $__env->make('partials/fc-content/global/cta-invoice-templates', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php elseif(get_row_layout() === 'two_col_small_image'): ?>
                <?php echo $__env->make('partials/fc-content/global/two-col-small-image', ['count' => $counter], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php endif; ?>

            <?php ($counter++); ?>
        <?php endwhile; ?>

    <?php endif; ?>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>