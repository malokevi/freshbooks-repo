<?php
    $custom_button_text = isset($signup_button_text) ? $signup_button_text : false;
    if (!empty(get_field('trial_length'))) {
        $trial_length = get_field('trial_length');
    } elseif (!empty(get_sub_field('trial_length'))) {
        $trial_length = get_sub_field('trial_length');
    } elseif (!empty(get_field('lpat_trial_length'))) {
        $trial_length = get_field('lpat_trial_length');
    } else {
        $trial_length = 30;
    }
    $trial_length_data_attribute = 'data-tl="'.$trial_length.'"';

    $abobeInsightsTrackingElement = isset($abobeInsightsTrackingElement) ? $abobeInsightsTrackingElement : '';
    $adobeInsightsExitLink = isset($adobeInsightsExitLink) ? $adobeInsightsExitLink : '';
    $actionDomain = App\getFormActionDomain() ?: 'my.freshbooks.com';
    if (!isset($enable_sso)) {
        $enable_sso = false;
    }
?>

<div class="inline-form-hero">
    <?php if($enable_sso): ?>
        <div id="signupContainer__googleSso" class="signupContainer__googleSso signupContainer__googleSso--hidden">
            <div id="googleSsoSignup" class="googleSsoBtn">
                <img class="icon" src="<?= App\asset_path('images/icons/google-g-logo.svg'); ?>">
                <span class="text"><?php echo e(__('Sign Up With Google', 'freshpress-theme')); ?></span>
            </div>
            <div class="orDivider">
                <div class="lineDivider left"></div>
                <?php echo e(_x('OR', 'Signup method separator', 'freshpress-theme')); ?>

                <div class="lineDivider right"></div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(get_field('form_error_message', 'option')): ?>
        <div class="errorGeneral"><?php echo e(get_field('form_error_message', 'option')); ?></div>
    <?php endif; ?>

    <form class="inline-form-subtext-assurance" id="inline-form-hero" <?php echo $trial_length_data_attribute; ?>  data-ep="https://<?php echo $actionDomain; ?>/auth/api/v1/smux/registrations" method="post" novalidate>
        <?php if(get_sub_field('cta_side_image')['enable_side_image']): ?>
            <div class="cta-side-mobile">
                <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('cta_side_image')['side_image'], 'caption' => get_sub_field('cta_side_image')['caption'], 'description' => get_sub_field('cta_side_image')['description']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        <?php endif; ?>
        <div class="fieldset fieldset-email">
            <input id="inline_email_hero" type="email" name="email" placeholder="<?php echo e(__('Enter Your Email', 'freshpress-theme')); ?>" maxlength="50" class="<?php echo e($abobeInsightsTrackingElement); ?> form-input input-email" autocomplete="off" required>
            <?php if(get_field('invalid_email_error_message', 'option')): ?>
                <div class="formError error-email errorState">
                    <div class="errorMessage clientMessage">
                        <?php echo e(get_field('invalid_email_error_message', 'option')); ?>

                    </div>
                    <div class="errorMessage serverMessage"></div>
                </div>
            <?php endif; ?>
        </div>
        <div class="fieldset fieldset-password">
            <input id="inline_password_hero" type="password" name="password" placeholder="<?php echo e(__('Create a Password (min 8 characters)', 'freshpress-theme')); ?>" class="form-input input-password" minlength="8" autocomplete="new-password" required>
            <?php if(get_field('invalid_password_error_message', 'option')): ?>
                <div class="formError error-password errorState">
                    <div class="errorMessage clientMessage">
                        <?php echo e(get_field('invalid_password_error_message', 'option')); ?>

                    </div>
                    <div class="errorMessage serverMessage"></div>
                </div>
            <?php endif; ?>
        </div>
        <div class="cta-subtext">
            <div class="submit-assurance">
                <button id="signup-cta" type="submit" class="primary-cta <?php echo e($adobeInsightsExitLink); ?>">
                    <?php if($custom_button_text || get_field('submit_button_text', 'option')): ?>
                        <span><?php echo e($custom_button_text ?: get_field('submit_button_text', 'option')); ?></span>
                    <?php endif; ?>
                </button>
                <?php if(get_sub_field('cta_side_image')['enable_side_image']): ?>
                    <div class="side-image-container">
                        <?php if(get_sub_field('cta_side_image')['caption']): ?>
                            <p class="side-image-caption">
                                <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('cta_side_image')['side_image'], 'caption' => get_sub_field('cta_side_image')['caption'], 'description' => get_sub_field('cta_side_image')['description']] , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </p>
                        <?php else: ?>
                            <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('cta_side_image')['side_image']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php endif; ?>
                    </div>
                <?php elseif(get_field('form_side_image', 'option')): ?>
                    <?php echo $__env->make('partials.components.global-image', ['img' => get_field('form_side_image', 'option')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endif; ?>
            </div>
            <?php echo get_field('form_subtext', 'option'); ?>

            <?php echo $__env->make('partials.fc-content.web-segmentation.schedule-demo-link', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </form>
</div>
