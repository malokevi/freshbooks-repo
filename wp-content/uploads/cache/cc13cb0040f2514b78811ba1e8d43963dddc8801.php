<?php $__env->startSection('content'); ?>

<div class="container">
  <section class="404-wrapper ">
      <div class="four-oh-four-copy">
        <?php echo get_field('404_content', 'option'); ?>

      </div>
      <div class="four-oh-four-image">
        <a href="<?php echo e(get_field('404_image_link', 'option') ? get_field('404_image_link', 'option') : 'javascript:;'); ?>" target="_blank" rel="noreferer">
          <?php if(get_field('404_image', 'option')): ?>
            <?php echo $__env->make('partials.components.global-image', ['img' => get_field('404_image', 'option') ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endif; ?>
        </a>
      </div>
      <div class="clear-both"></div>
		  <div id="st-results-container"></div>
    </section>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>