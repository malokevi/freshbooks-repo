<div class="container no-side-pad anchored-content content-<?php echo e(get_sub_field('layout')); ?>">
	<section class="support">
		<div class="two-col">
			<div class="col">
				<?php if(get_sub_field('two-col-content_header')): ?>
					<h2><?php echo get_sub_field('two-col-content_header'); ?></h2>
				<?php endif; ?>
				<?php echo get_sub_field('two-col-content'); ?>

				<?php if(get_sub_field('contact_support_heading')): ?>
					<p class="support-heading"><strong><?php echo e(get_sub_field('contact_support_heading')); ?></strong></p>
				<?php endif; ?>
				<div class="contact-details">
					<?php if(get_sub_field('support_phone')): ?>
						<p><a><span class="phone"><?php echo e(get_sub_field('support_phone')); ?></span></a></p>
					<?php endif; ?>
					<?php if(get_sub_field('support_email')): ?>
						<p><a href="mailto:<?php echo e(get_sub_field('support_email')); ?>"><span class="email"><?php echo e(get_sub_field('support_email')); ?></span></a></p>
					<?php endif; ?>
				</div>
			</div>
			<div class="col">
				<?php if(get_sub_field('two-col-image')): ?>
					<?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('two-col-image'), 'classes' => 'content-img ' . (get_sub_field('image_no_padding') ? 'no-pad' : '') . (get_sub_field('custom_image_class') ? get_sub_field('custom_image_class') : '')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>