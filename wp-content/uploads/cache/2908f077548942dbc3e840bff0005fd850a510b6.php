<?php
    $bg_img = get_sub_field('background_image') ?: get_sub_field('hero_background_image');
    $hero_title = get_sub_field('hero_title') ?: get_sub_field('hero_hero_title');

    //  ---------------------- Grab data for Single Column Layout partial
    $url['button_text'] = get_sub_field('button_one');
    $text['hero_text'] = get_sub_field('hero_text');      
    $text['button_text'] = get_sub_field('button_text') ? get_sub_field('button_text') : '';
    $text['offer_details_heading'] = get_sub_field('offer_details_heading');
    $text['offer_details_text'] = get_sub_field('offer_details_text');

    $has_anchor = get_sub_field('price_table_anchor');
    
    if($has_anchor) {
        $text['anchor_text'] = get_sub_field('anchor_text') ? get_sub_field('anchor_text') : '';      
        $class['anchor_id'] = get_sub_field('anchor_id') ? get_sub_field('anchor_id') : '';
    }
?>

<style>
  #instant-invoice-hero.bg-img {
    background-image: url(<?php echo e($bg_img); ?>);
  }
</style>

<div id="instant-invoice-hero" class="container bg-img hero--single-column instant-invoice-hero">
  	<section id="cpy-hero" class="">
        <div class="content">
            <?php if($hero_title): ?>
                <div class="hero-intro-heading">
                    <?php if(get_sub_field('highlight')): ?>
                      <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('highlight'), 'classes' => 'highlights', 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                    <h1><?php echo $hero_title; ?></h1>
                </div>
            <?php endif; ?>


            <div class="hero-single-column">
                <div class="col">
                    <p><?php echo e($text['hero_text']); ?></p>

                    <div class="single-column-buttons">
                        <a href="<?php echo e($url['button_text']); ?>" class="<?php echo e($has_anchor ? '' : 'button--mr-fix'); ?> primary-cta"><?php echo e($text['button_text']); ?></a>        
                        <?php if($has_anchor): ?> 
                            <div class="button__group">
                                <a href="#<?php echo e($class['anchor_id']); ?>" class="ghost-button"><?php echo e($text['anchor_text']); ?></a>
                                <span class="pricingHero__link">
                                <?php echo e(__('See Offer Details', 'freshpress-theme')); ?> *
                                </span>
                                <div class="pricingHero__details">
                                    <h2 class="pricingHero__detailsHeading"><?php echo e($text['offer_details_heading']); ?></h2>
                                    <div class="pricingHero__detailsDescription">
                                        <p><?php echo e($text['offer_details_text']); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
	</section>
</div>
