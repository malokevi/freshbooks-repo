<header>
    <?php ($page_template = basename(get_page_template())); ?>
    <?php if(get_field('use_header_banner')): ?>
        <?php echo $__env->make('partials.header.header-banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <?php if(!get_field('override_wp_header')): ?>
        <?php if(
            (!is_404() && get_field('use_custom_header')) ||
            App\is_post_type('invoice_templates') || is_tax('invoice_templates_categories') ||
            App\is_post_type('accounting_software') || is_tax('accounting_software_categories') ||
            App\is_post_type('lpat_pages') ||
            App\is_post_type('api') ||
            App\is_post_type('education') ||
            App\is_post_type('support') ||
            is_tax('support_categories') ||
            $page_template === 'support-page.blade.php' ||
            $page_template === 'accounting-templates-page.blade.php' ||
            App\is_post_type('accounting_templates') ||
            is_tax('accounting_templates_categories')
            ): ?>
            <?php if( (App\is_post_type('support') || is_tax('support_categories') || $page_template === 'support-page.blade.php')  && get_field('support_display_classic_bar', 'option') ): ?>
                <?php echo $__env->make('partials.header.sticky-bar', ['link_url' => is_single() ? get_field('paired_article_url') : 'https://support.freshbooks.com'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
            <?php echo $__env->make('partials.header.custom-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif(App\is_post_type('addons') || is_tax('addons_categories') || $page_template === 'addons.blade.php' || App\is_post_type('developers') || $page_template === 'developers-page.blade.php'): ?>
            <?php echo $__env->make('partials.header.sticky-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('partials.components.global-classic-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif($page_template === 'benefits.blade.php' || $page_template === 'business-name-generator.blade.php'): ?>
            <?php echo $__env->make('partials.components.global-classic-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif($page_template === 'annual-report.blade.php'): ?>
            <?php echo $__env->make('partials.annual-report.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif($page_template === 'select.blade.php'): ?>
            <?php echo $__env->make('partials.select.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif($page_template === 'brand-campaign.blade.php'): ?>
            <?php echo $__env->make('partials.brand-campaign.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif($page_template === 'info-hub-page.blade.php'): ?>
            <?php echo $__env->make('partials.info-hub-page.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif(App\is_post_type('resources') || $page_template === 'resources-page.blade.php'): ?>
            <?php echo $__env->make('partials.components.global-header',[
                'menu' => 'resources'
            ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php elseif($page_template === 'targeted-segment-page.blade.php' || $page_template === 'instant-invoice-pricing-table.blade.php'): ?>
            <?php echo $__env->make('partials.header.minimal-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php else: ?>
            <?php echo $__env->make('partials.components.global-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    <?php endif; ?>
</header>
