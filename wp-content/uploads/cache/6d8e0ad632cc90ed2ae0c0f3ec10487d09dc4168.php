<?php
	// Page Values
	$id = get_sub_field('title') ? sanitize_title_with_dashes(strtolower(get_sub_field('title'))) : 'cpy-cta-full-width';
?>

<div class="container no-side-pad cta-full-wrap">
    <section class="cta-2-full-width" id="<?php echo e($id); ?>">
        <div class="two-col">
            <div class="col">
                <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('left_side_image'), 'lazy_load' =>
                false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col">
                <div class="cta-callout">
                    <h2 class="cta__heading"><?php echo e(get_sub_field('callout_title')); ?></h2>

                    <div class="cta__button">
                        <a href="<?php echo e(get_field('signup_url')); ?>" class="primary-cta full-width-cta"
                            rel="noopener"><?php echo e(get_sub_field('button_text') ? get_sub_field('button_text') : get_field('cta_title', 'option')); ?></a>
                    </div>

                    <p class="cta__disclaimer"><?php echo e(get_sub_field('callout_disclaimer')); ?></p>
                </div>
            </div>
        </div>
    </section>
</div>