<?php
$include_icons = isset($include_icons) ? $include_icons : get_sub_field('include_icons');
$icons = isset($icons) ? $icons : get_sub_field('icons');
?>

<?php if($include_icons && have_rows('icons')): ?>
    <div class="heroIcons">
        <?php while(have_rows('icons')): ?> <?php (the_row()); ?>
            <span class="heroIcons__icon<?php echo e(get_sub_field('hide_on_mobile') ? ' hideOnMobile' : ''); ?>">
                <img class="heroIcons__iconImage" src="<?php echo e(get_sub_field('icon_image')); ?>" alt="">
                <?php echo get_sub_field('icon_text'); ?>

            </span>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
