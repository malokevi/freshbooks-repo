<?php
    $embed = isset($embed) && $embed === true ? true : false;
    $text = get_field('review_text', 'option') ?: __('Excellent', 'freshpress-theme');
    $rating = get_field('review_rating', 'option')['url'] ?: App\asset_path('images/testimonial/four-point-five-stars.svg');
    $review_count = number_format(get_field('review_count', 'option')); // NOT related to counter (for section IDs)
    $section = isset($count) ? "section" . $count : "";
?>

<?php if(!$embed): ?>
<div id="<?php echo e($section); ?>" class="getapp container">
    <section>
<?php endif; ?>
        <div class="getapp-widget <?php echo e($embed ? 'embedded' : ''); ?>">
            <strong><?php echo e($text); ?></strong>
            <img src="<?php echo e($rating); ?>" alt="<?php echo e(__('breadcrumb-arrow', 'freshpress-theme')); ?>" class="arrow">
            <span class="nowrap">
            <?php echo sprintf(_x('(Based on <a target="_blank" rel="nofollow" href="https://www.getapp.com/finance-accounting-software/a/freshbooks/">%1$s GetApp reviews</a>)', 'Get App Review Count', 'freshpress-theme'), $review_count); ?>

            </span>
        </div>
<?php if(!$embed): ?>
    </section>
</div>
<?php endif; ?>
