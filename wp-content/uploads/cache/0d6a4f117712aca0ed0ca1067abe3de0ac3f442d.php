<?php echo $__env->make('partials.fc-content.global.divider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container no-side-pad-tablet">
  <div class="footer-nav">
    <div class="footer-col">
      <a href="<?php echo e(home_url('/')); ?>">
        <img src="<?= App\asset_path('images/navigation/default-logo.svg'); ?>" alt="FreshBooks Cloud Accounting" class="footer-logo">
      </a>
    </div>
    <?php
      $location = 'footer-col-1';
    ?>
    <?php if(has_nav_menu($location)): ?>
      <div class="footer-col">
        <span class="footer-top-level"><?php echo e(__('Company', 'Footer menu title', 'freshpress-theme')); ?></span>
        <div class="footer-sub-links">
            <?php echo wp_nav_menu([
              'theme_location' => $location,
              'container'=> false,
              'menu_class'=> false,
            ]); ?>

        </div>
      </div>
    <?php endif; ?>

    <?php
      $location = 'footer-col-2';
    ?>
    <?php if(has_nav_menu($location)): ?>
      <div class="footer-col">
        <span class="footer-top-level"><?php echo e(__('Product', 'Footer menu title', 'freshpress-theme')); ?></span>
        <div class="footer-sub-links">
            <?php echo wp_nav_menu([
              'theme_location' => $location,
              'container'=> false,
              'menu_class'=> false,
            ]); ?>

        </div>
      </div>
    <?php endif; ?>

    <?php
      $location = 'footer-col-3';
    ?>
    <div class="footer-col">
      <?php if(has_nav_menu($location)): ?>
        <span class="footer-top-level"><?php echo e(__('Who it\'s for', 'Footer menu title', 'freshpress-theme')); ?></span>
        <div class="footer-sub-links">
            <?php echo wp_nav_menu([
              'theme_location' => $location,
              'container'=> false,
              'menu_class'=> false,
            ]); ?>

        </div>
      <?php endif; ?>

      <?php
        $location = 'footer-col-4';
      ?>
      <?php if(has_nav_menu($location)): ?>
        <span class="footer-top-level second-row"><?php echo e(__('Partners', 'Footer menu title', 'freshpress-theme')); ?></span>
        <div class="footer-sub-links">
            <?php echo wp_nav_menu([
              'theme_location' => $location,
              'container'=> false,
              'menu_class'=> false,
            ]); ?>

        </div>
      <?php endif; ?>
    </div>

    <?php
      $location = 'footer-col-5';
    ?>
    <?php if(has_nav_menu($location)): ?>
    <div class="footer-col">
      <span class="footer-top-level"><?php echo e(__('Helpful Links', 'Footer menu title', 'freshpress-theme')); ?></span>
      <div class="footer-sub-links">
          <?php echo wp_nav_menu([
            'theme_location' => $location,
            'container'=> false,
            'menu_class'=> false,
          ]); ?>

        </div>
      </div>
    <?php endif; ?>

    <?php
      $location = 'footer-col-6';
    ?>
    <?php if(has_nav_menu($location)): ?>
      <div class="footer-col">
        <span class="footer-top-level"><?php echo e(__('Policies', 'Footer menu title', 'freshpress-theme')); ?></span>
        <div class="footer-sub-links">
            <?php echo wp_nav_menu([
              'theme_location' => $location,
              'container'=> false,
              'menu_class'=> false,
            ]); ?>

          </div>
      </div>
    <?php endif; ?>
  </div>
</div>
