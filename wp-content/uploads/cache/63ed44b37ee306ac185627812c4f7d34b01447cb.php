<div id="modalCookies" class="banner banner-popup modalCookies--hidden modalCookies">
    <div class="card">
        <div class="content">
            <div class="col">
                <h2 class="header"><?php echo e(_x('Optional cookies and other technologies', 'Cookies acceptance modal', 'freshpress-theme')); ?></h2>
                <div class="body">
                    <p><?php echo e(_x('We use analytics cookies to ensure you get the best experience on our website. You can decline analytics cookies and navigate our website, however cookies must be consented to and enabled prior to using the FreshBooks platform. To learn about how we use your data, please Read our Privacy Policy. Necessary cookies will remain enabled to provide core functionality such as security, network management, and accessibility. You may disable these by changing your browser settings, but this may affect how the website functions.', 'Cookies acceptance modal', 'freshpress-theme')); ?></p>
                    <p><?php echo _x('To learn more about how we use your data, please read our <a href="/policies/privacy" class="modalCookies__link">Privacy Statement</a>.', 'Cookies acceptance modal', 'freshpress-theme'); ?></p>
                    <button type="button" class="modalCookies__button modalCookies__buttonAccept" id="modalCookies__buttonAccept"><?php echo e(_x('I Accept', 'Cookies acceptance modal', 'freshpress-theme')); ?></button>
                    <a class="modalCookies__buttonDecline" id="modalCookies__buttonDecline"><?php echo e(_x('No, Thank You', 'Cookies acceptance modal', 'freshpress-theme')); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
