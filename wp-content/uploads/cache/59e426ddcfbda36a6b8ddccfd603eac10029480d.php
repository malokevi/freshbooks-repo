<div class="container">
    <section class="addons column-parent" id="cpy-addons">
        <div class="two-col-parent-col content">
            <?php if(get_sub_field('addons_title')): ?>
                <h2><?php echo e(get_sub_field('addons_title')); ?> </h2> 
            <?php endif; ?>				
            <?php echo get_sub_field('addons_content'); ?>

            <?php if(get_sub_field('cta')): ?>
                <?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('cta'), 'classes' => 'ghost-button no-width'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?> 
        </div>
        <div class="two-col-parent-col addons-img">
            <?php if(get_sub_field('addons_image')): ?>
                <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('addons_image'), 'classes' => 'content-img'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        </div>
    </section>
</div>  