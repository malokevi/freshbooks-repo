<?php
    $currentRegion = App\parseLanguageCode(App\getCurrentLanguage())['country'];
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="msvalidate.01" content="ED10DA14718BD57C514D94385B93BC33" />
    <meta name="apple-itunes-app" content="app-id=1052884030" />
    <?php if($currentRegion === 'gb'): ?>
        
        <meta name="google-site-verification" content="EgSmC-cMR9ZuPv49H1YTV3bTYhgr7a5M8MmSSGAm-Sg" />
    <?php else: ?>
        
        <meta name="google-site-verification" content="Icc-RvQchUmbVcP7lUYbbWIU-KaNWp42hDOSP9rSfTo" />
    <?php endif; ?>
    <meta name="author" content="FreshBooks">
    <meta name="st:robots" content="follow,index">
    <link rel="preload" as="font" crossorigin type="font/woff" href="<?= App\asset_path('fonts/FranklinGothicURW-Lig.woff'); ?>" />
    <link rel="preload" as="font" crossorigin type="font/woff" href="<?= App\asset_path('fonts/FranklinGothicURW-Boo.woff'); ?>" />
    <link rel="preload" as="font" crossorigin type="font/woff" href="<?= App\asset_path('fonts/FranklinGothicURW-Med.woff'); ?>" />
    <link rel="preload" as="font" crossorigin type="font/woff" href="<?= App\asset_path('fonts/FreshBooksScriptWeb-Regular.woff'); ?>" />
    <?php (wp_head()); ?>
    <?php echo $__env->make('partials.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
