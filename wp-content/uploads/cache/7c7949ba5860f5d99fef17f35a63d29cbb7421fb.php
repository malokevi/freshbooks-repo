<?php
    $counter = 0;
    $roadCount = 0;    
?>

<?php $__env->startSection('content'); ?>

    <?php if(  have_rows('flexible_content')  ): ?>

    <?php while(have_rows('flexible_content')): ?> <?php (the_row()); ?>

    <?php if(  get_row_layout() === 'instant_invoice_hero'  ): ?>
     <?php echo $__env->make('partials/fc-content/landers/instant-invoice-hero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     
      <?php elseif(  get_row_layout() === 'two_col_image_content'  ): ?>
        <?php if($roadCount == 0): ?><div class="roadmap"><?php endif; ?>
        <?php echo $__env->make('partials/fc-content/landers/two-col-image-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php if($roadCount == 2): ?></div><?php endif; ?>
        <?php ($roadCount++); ?>
      
      <?php elseif(  get_row_layout() === 'instant_invoice_cta'  ): ?>
        <?php echo $__env->make('partials/fc-content/landers/instant-invoice-cta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php elseif(  get_row_layout() === 'join_now_cta'  ): ?>
        <?php echo $__env->make('partials/fc-content/landers/join-now-cta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php elseif(  get_row_layout() === 'featured_in'  ): ?>
        <?php echo $__env->make('partials/fc-content/global/featured-in', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php elseif( get_row_layout() === 'three_column_content'  ): ?>
        <?php echo $__env->make('partials/fc-content/landers/three-col-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php elseif(  get_row_layout() === 'testimonial_columns'  ): ?>
        <?php echo $__env->make('partials/fc-content/global/testimonial-columns', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
      <?php elseif(  get_row_layout() === 'instant_invoice_pricing_table'  ): ?>
        <?php echo $__env->make('partials/fc-content/landers/instant-invoice-pricing-table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
      <?php elseif(  get_row_layout() === 'powerful_features'  ): ?>
        <?php echo $__env->make('partials/fc-content/pricing/two-col-icon-text-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php elseif(  get_row_layout() === 'customer_support'  ): ?>
        <?php echo $__env->make('partials/fc-content/landers/customer-support', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php elseif(  get_row_layout() === 'lander_divider'  ): ?>
        <?php echo $__env->make('partials/fc-content/landers/lander-divider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php endif; ?>

      <?php ($counter++); ?>
    <?php endwhile; ?>

    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>