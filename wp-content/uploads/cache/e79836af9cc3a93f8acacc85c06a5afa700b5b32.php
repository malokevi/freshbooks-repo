<?php
    // You can pass your own class into this partial or use .bg-img instead.
    $class = isset($class) ? '.' . $class : '.bg-img';
    $bg_img = isset($backgroundImages['desktop_standard_background_image']) ? $backgroundImages['desktop_standard_background_image'] : get_sub_field('desktop_standard_background_image');
    $retina_bg_img = isset($backgroundImages['desktop_retina_background_image']) ? $backgroundImages['desktop_retina_background_image'] : get_sub_field('desktop_retina_background_image');
    $rwd_bg_imgs = isset($backgroundImages['responsive_background_images']) ? $backgroundImages['responsive_background_images'] : get_sub_field('responsive_background_images');
    $single_mobile_image = get_sub_field('mobile_background_image');

    if (empty($bg_img)) {
        $bg_img = get_sub_field('background_image');
    }

    if (!empty($rwd_bg_imgs)) {
        usort($rwd_bg_imgs, function($a, $b) {
            return ($b['breakpoint'] === 'custom' ? $b['custom_breakpoint'] : $b['breakpoint']) - ($a['breakpoint'] === 'custom' ? $a['custom_breakpoint'] : $a['breakpoint']);
        });
    }
?>

<style>
    <?php echo e($class); ?> {
        background-image: url('<?php echo e($bg_img); ?>');
    }

    <?php if(!empty($single_mobile_image)): ?>
        @media  screen and (min-width: 320px) and (max-width: 480px) {
            <?php echo e($class); ?> {
                background-image: url('<?php echo e($single_mobile_image); ?>');
            }
        }
    <?php endif; ?>

    <?php if(!empty($retina_bg_img)): ?>
        @media  only screen and (-webkit-min-device-pixel-ratio: 1.25), only screen and ( min-device-pixel-ratio: 1.25) {
            <?php echo e($class); ?> {
                background-image: url('<?php echo e($retina_bg_img); ?>');
            }
        }
    <?php endif; ?>

    <?php if(!empty($rwd_bg_imgs)): ?>
        <?php $__currentLoopData = $rwd_bg_imgs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php ($breakpoint = $img['breakpoint'] === 'custom' ? $img['custom_breakpoint'] : $img['breakpoint']); ?>
            <?php if(!empty($img['standard_image'])): ?>
                @media  screen and (max-width: <?php echo e($breakpoint); ?>px) {
                    <?php echo e($class); ?> {
                        background-image: url('<?php echo e($img['standard_image']); ?>');
                    }
                }
            <?php endif; ?>

            <?php if(!empty($img['retina_image'])): ?>
                @media  only screen and  ( min-width: <?php echo e($breakpoint); ?>px) and (-webkit-min-device-pixel-ratio: 1.25), only screen and ( min-width: <?php echo e($breakpoint); ?>px) and ( min-device-pixel-ratio: 1.25) {
                    <?php echo e($class); ?> {
                        background-image: url('<?php echo e($img['retina_image']); ?>');
                    }
                }
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
</style>
