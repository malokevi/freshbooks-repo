<div class="container">
    <section class="who-its-for column-parent">
        <a id="section<?php echo e($counter); ?>" class="anchor"></a>
        <?php if(have_rows('two_col_content')): ?>
            <?php while(have_rows('two_col_content')): ?> <?php (the_row()); ?>
                <div class="who-its-for-col">
                    <?php if(get_sub_field('column_title')): ?>
                        <h2><?php echo get_sub_field('column_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_sub_field('column_image')): ?>
                        <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('column_image')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>

                    <?php echo get_sub_field('column_description'); ?>


                    <?php if(get_sub_field('column_cta')): ?>
                        <?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('column_cta'), 'classes' => 'ghost-button learn-more'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
	</section>
</div>
