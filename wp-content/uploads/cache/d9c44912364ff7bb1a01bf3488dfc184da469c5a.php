<?php

/**
* @partial, displays image and retina image based on params given
* @param $img[] array from ACF
* @param $classes custom classes to include on image element
*/
	$image = isset($img['global_image']['url']) ? $img['global_image']['url'] : get_sub_field('global_image')['url'];
	$has_retina = isset($img['global_include_retina_image']) ? $img['global_include_retina_image'] : get_sub_field('global_include_retina_image');
	$retina_image = isset($img['global_retina_image']['url']) ? $img['global_retina_image']['url'] : get_sub_field('global_retina_image')['url'];
	$lazy = isset($lazy_load) ? $lazy_load : (isset($img['global_disable_lazy_load']) ? !$img['global_disable_lazy_load'] : !get_sub_field('global_disable_lazy_load'));
	$display_as_background = isset($is_background_image) ? $is_background_image : 'false';
	$background_element_id = isset($background_element_id) ? $background_element_id : '';
	$title = isset($img['global_image']['title']) ? preg_replace("/[^a-zA-Z]+/", "", $img['global_image']['title']) : false;
	$caption = isset($caption) ? $caption : '';
	$description = isset($description) ? $description : '';

	$classes = isset($img['custom_image_class']) ?
		$img['custom_image_class'] . ' ' . (isset($classes) ? $classes :
		get_sub_field('custom_image_class')) :
		(isset($classes) ? $classes : '');

	if (isset($img['global_image']['alt'])) {
		if ($retina_image) {
			$alt = $img['global_retina_image']['alt'];
		} else {
			$alt = $img['global_image']['alt'];
		}
	} elseif (isset(get_sub_field('global_image')['alt'])) {
		if ($retina_image) {
			$alt = get_sub_field('global_retina_image')['alt'];
		} else {
			$alt = get_sub_field('global_image')['alt'];
		}
	}

?>
<?php if($image): ?>
	<?php if($display_as_background == 'true'): ?>
		<style>
		<?php echo e("#".$background_element_id); ?>{
			background-image: url(<?php echo e($image); ?>);
		}
        <?php if($retina_image): ?>
    		@media  all and (-webkit-min-device-pixel-ratio : 1.5),
    	 		all and (-o-min-device-pixel-ratio: 3/2),
    	 		all and (min--moz-device-pixel-ratio: 1.5),
    	 		all and (min-device-pixel-ratio: 1.5) {
    				<?php echo e("#".$background_element_id); ?>{
    					background-image: url(<?php echo e($retina_image); ?>);
    				}
        <?php endif; ?>
		</style>
	<?php else: ?>
		<img <?php echo $lazy ? 'data-src=' . $image . ' src=""' : 'src="'. $image . '"'; ?> <?php echo $title ? 'id="' . $title . '"' : ''; ?> class="<?php echo e($lazy ? 'lazy' : ''); ?> <?php echo e($classes); ?>" alt="<?php echo e($alt); ?>" <?php echo $has_retina == 1 ? ($lazy ? 'data-' : '') . 'srcset="' . $image . ' 1x, ' . $retina_image . ' 2x"' : ''; ?>>
		<?php if($caption): ?>
			<span class="image-caption">
                <?php echo e($caption); ?>

                <?php if($description): ?>
                    <span class="image-caption-description"><?php echo e($description); ?></span>
                <?php endif; ?>
            </span>
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>
