<?php
    $nfbCustomerVisibility = 'data-nfb-visibility=' . $settings['nfb_customer_visibility'];
?>
<div id="<?php echo e($bid); ?>" class="banner banner-sm banner-blue banner-bottom banner-hidden banner-cookies" <?php echo $nfbCustomerVisibility; ?>>
    <div class="banner-sm-copy">
        <div class="copy"><?php echo $content['simple_bottom_content'] ?: ""; ?></div>
        <div class="banner__buttonContainer">
            <?php if($content['auxiliary_bottom_close_button_text']): ?>
            <button id="banner-acceptCookies" class="banner-cookie-cta banner-close banner-accept-cookies"><?php echo e($content['auxiliary_bottom_close_button_text']); ?></button>
            <?php endif; ?>
            <?php if($content['auxiliary_bottom_decline_cookies_button_text']): ?>
            <button id="banner-declineCookies" class="banner-cookie-cta banner-decline-cookies"><?php echo e($content['auxiliary_bottom_decline_cookies_button_text']); ?></button>
            <?php endif; ?>
        </div>
    </div>
</div>
