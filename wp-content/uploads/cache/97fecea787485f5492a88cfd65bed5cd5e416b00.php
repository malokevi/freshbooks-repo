<?php
	// Page Values
	$id = get_sub_field('title') ? sanitize_title_with_dashes(strtolower(get_sub_field('title'))) : 'cpy-cta-full-width';
	$phone = get_sub_field('contact_phone') ? get_sub_field('contact_phone') : null;
	$email = get_sub_field('contact_email') ? get_sub_field('contact_email') : null;
	$has_support = false;

	if($phone != null && $email != null)
	{
		$has_support = true;
	}
?>

<div class="container no-side-pad customerSupport__block">
	<section id="<?php echo e($id); ?>">
		<div class="two-col">
			<div class="col">
				<?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('left_side_image'), 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
			<div class="col">
				<h2 class="cta__heading"><?php echo e(get_sub_field('title') ? get_sub_field('title') : get_field('cta_title', 'option')); ?></h2>
				<div class="cta__description">
					<p><?php echo get_sub_field('caption') ? get_sub_field('caption') : get_field('cta_subtext', 'option'); ?></p>
				</div>
				<?php if($has_support == true): ?>
					<div class="contact-details">
							<a href="tel:<?php echo e($phone); ?>" target="_blank"><span class="phone"><?php echo e($phone); ?></span></a>
							<a href="mailto:<?php echo e($email); ?>" target="_blank"><span class="email"><?php echo e($email); ?></span></a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>
