<?php
    $dataAttrVals = App\data_attributes($settings['data_attributes']);
    $hasCountdown = (strpos($content['custom']['custom_banner'], 'class="globalCountdown') !== false) ? 'hasCountdown' : '';
    $strippedContent = str_replace(['<p>', '</p>'], '', $content['custom']['custom_banner']);
    $nfbCustomerVisibility = 'data-nfb-visibility=' . $settings['nfb_customer_visibility'];
    $backgroundImage = $content['custom']['banner_background_image'];
?>
<style>
    .bannerCustom {
        background-color: <?php echo e($content['custom']['background_color']); ?>;
    }

    .bannerCustom__copy h1,
    .bannerCustom__copy h2,
    .bannerCustom__copy h3,
    .bannerCustom__copy h4,
    .bannerCustom__copy h5,
    .bannerCustom__copy h6,
    .bannerCustom__copy p {
        color: <?php echo e($content['custom']['font_color']); ?>;
    }

    .bannerCustom__copy .bannerCustom__btn {
        border: 2px solid <?php echo e($content['custom']['font_color']); ?>;
        color: <?php echo e($content['custom']['font_color']); ?>;
    }

    .bannerCustom__copy .bannerCustom__btn:hover {
        background-color: <?php echo e($content['custom']['font_color']); ?> !important;
    }
</style>
<?php if($backgroundImage): ?>
    <?php echo $__env->make('partials.components.global-background-image', ['backgroundImages' => $backgroundImage, 'class' => 'bannerBg'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>


<div id="<?php echo e($bid); ?>" class="banner banner-blue bannerCustom banner-top bannerSlideIn <?php echo e($content['custom']['fixed_full_width'] ? ' bannerFixed' : ''); ?> <?php echo e($hasCountdown); ?><?php echo e($backgroundImage ? ' bannerBg' : ''); ?>" <?php echo $dataAttrVals; ?> <?php echo $nfbCustomerVisibility; ?>>
    <div class="bannerCustom__copy <?php echo e($content['custom']['allow_overflow'] ? 'overflow' : ''); ?>">
        <a href="/pricing" class="mobileClickable"></a>
        <?php echo $strippedContent; ?>

    </div>
    <?php if($content['custom']['custom_element']): ?>
        <?php echo $__env->make('partials.global.notification-banners.custom-element', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
</div>
