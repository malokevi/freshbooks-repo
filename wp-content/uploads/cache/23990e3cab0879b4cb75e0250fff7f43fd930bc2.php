<div class="container container--body pt__4">
	<section class="pillars three-col-content">
		<a id="section<?php echo e($counter); ?>" class="anchor"></a>
		<?php if(have_rows('three_col_content')): ?>
			<div class="three-col">
				<?php while(have_rows('three_col_content')): ?> <?php (the_row()); ?>
					<div class="col">
						<?php if(get_sub_field('column_image')): ?>
							<div class="image__frame">
								<?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('column_image'), 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							</div>
						<?php endif; ?>
						<?php if(get_sub_field('column_title')): ?>
							<h3><?php echo e(get_sub_field('column_title')); ?></h3>
						<?php endif; ?>
						<?php echo get_sub_field('column_content'); ?>

					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</section>
</div>
