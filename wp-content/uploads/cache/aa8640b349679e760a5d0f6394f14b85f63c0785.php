<div class="desktop-nav">
    <div class="container">
        <a href="<?php echo e(home_url('/')); ?>" class="nav-logo">
            <img src="<?= App\asset_path('images/navigation/default-logo.svg'); ?>" alt="FreshBooks Cloud Accounting" width="120">
        </a>
        <div class="primary-nav">
            <div class="core">
                <div class="button-nav button-nav--minimal">
                    <?php if(get_field('login_link', 'option') && get_field('login_link') == true): ?>
                        <a href="<?php echo e(get_field('login_link', 'option')); ?>" class="top-level nav-item"><?php echo e(get_field('login_link_text', 'option')); ?></a>
                    <?php endif; ?>
		    
		            <?php if(get_field('sign_up_link', 'option')): ?>
                        <a class="top-level nav-item ghost-button goal-header-cta" href="<?php echo e(get_field('sign_up_link', 'option')); ?>"><?php echo e(get_field('sign_up_link_text', 'option')); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="mobile-nav">
    <div class="mobile-nav-bar">
        <a href="<?php echo e(home_url('/')); ?>" class="nav-logo">
            <img src="<?= App\asset_path('images/navigation/default-logo.svg'); ?>" alt="FreshBooks Cloud Accounting" width="100">
        </a>
        <div class="mobile-buttons">
            <?php if(get_field('login_link', 'option') && get_field('login_link') == true): ?>
                <a href="<?php echo e(get_field('login_link', 'option')); ?>" class="top-level nav-item contact-num"><?php echo e(get_field('login_link_text', 'option')); ?></a>
            <?php endif; ?>

            <?php if(get_field('sign_up_link', 'option')): ?>
                <a href="<?php echo e(get_field('sign_up_link', 'option')); ?>" class="btn-primary">
                    <span><?php echo e(get_field('sign_up_link_text', 'option')); ?></span>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
