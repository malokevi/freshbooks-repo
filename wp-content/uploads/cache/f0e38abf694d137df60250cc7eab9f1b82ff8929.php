
<?php echo App\getGlobalJsVars(); ?>



<?php if(defined('WP_ENV') && (WP_ENV === 'production' || WP_ENV === 'staging')): ?>
    
    <?php if(App\cookiesAccepted()): ?>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MCH68J');</script>
    <?php endif; ?>
    
    <?php echo App\getPageSpecificScripts(); ?>

<?php endif; ?>


<?php echo App\getPageSpecificStyles(); ?>



<?php echo App\getSchemaMarkup(); ?>

