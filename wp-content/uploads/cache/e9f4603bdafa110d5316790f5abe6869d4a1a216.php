<?php
    $appleClasses = isset($app_store_classes) ? $app_store_classes : '';
    $googleClasses = isset($play_store_classes) ? $play_store_classes : '';
?>

<?php if(get_field('apple_app_store', 'option')): ?>
    <?php
        $appStore = get_field('apple_app_store', 'option');
        $appStoreUrl = isset($appleUrl['app_store_url']) ? $appleUrl['app_store_url'] : $appStore['app_store_url'];
        $appStoreIcon = isset($appleIcon['global_link_text']) ? $appleIcon['global_link_text'] : $appStore['app_store_icon'];
    ?>
<?php endif; ?>

<?php if(get_field('google_play_store', 'option')): ?>
    <?php
        $playStore = get_field('google_play_store', 'option');
        $googleStoreUrl = isset($googleUrl['global_cta_subtext']) ? $googleUrl['global_cta_subtext'] : $playStore['google_play_url'];
        $googleStoreIcon = isset($googleUrl['global_link_action']) ? $googleUrl['global_link_action'] : $playStore['google_play_icon'];
    ?>
<?php endif; ?>

<?php if($appStoreUrl): ?>
    <a href="<?php echo e($appStoreUrl); ?>" class="<?php echo e($appleClasses); ?>" target="_blank" rel="noopener" title="<?php echo e(__('App Store', 'freshpress-theme')); ?>">
        <?php if($appStoreIcon): ?>
            <img src="<?php echo e($appStoreIcon['url']); ?>" alt="<?php echo e($appStoreIcon['alt']); ?>">
        <?php endif; ?>
    </a>
<?php endif; ?>

<?php if($googleStoreUrl): ?>
    <a href="<?php echo e($googleStoreUrl); ?>" class="<?php echo e($googleClasses); ?>" target="_blank" rel="noopener" title="<?php echo e(__('Google Play', 'freshpress-theme')); ?>">
        <?php if($googleStoreIcon): ?>
            <img src="<?php echo e($googleStoreIcon['url']); ?>" alt="<?php echo e($googleStoreIcon['alt']); ?>">
        <?php endif; ?>
    </a>
<?php endif; ?>
