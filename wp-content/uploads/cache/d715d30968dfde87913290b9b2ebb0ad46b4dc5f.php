<div class="container feature-list" id="feature-list">
	<section>
        <?php if(get_sub_field('heading')): ?>
		    <h2><?php echo e(get_sub_field('heading')); ?></h2>
        <?php endif; ?>
        <?php if(have_rows('icon_list')): ?>
			<ul class="list-items">
                <?php while(have_rows('icon_list')): ?> <?php (the_row()); ?>
                    <li style="background-image: url('<?php echo e(get_sub_field('list_item_icon')['url']); ?>');"><?php echo e(get_sub_field('list_item_text')); ?></li>
                <?php endwhile; ?>
			</ul>
        <?php endif; ?>
	</section>
</div>