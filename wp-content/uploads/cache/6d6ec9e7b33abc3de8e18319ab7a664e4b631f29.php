<?php
    // Make the variables work for the original or clones containing appended field names
    $hero_title = get_sub_field('hero_title') ?: get_sub_field('hero_hero_title');
    $hero_description = get_sub_field('hero_description') ?: get_sub_field('hero_hero_description');
    $include_reg_form = !empty(get_sub_field('include_reg_form')) ?: get_sub_field('hero_include_reg_form');
    $include_slide_in_reg_form = !empty(get_sub_field('include_slide_in_reg_form')) ?: get_sub_field('hero_include_slide_in_reg_form');
    $section = isset($count) ? "section" . $count : '';
    $getapp = get_sub_field('include_getapp_widget') ?: false;
    $custom_class = get_sub_field('hero_custom_class') ? ' ' . get_sub_field('hero_custom_class') : '';
?>

<?php echo $__env->make('partials/components/global-background-image', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div id="<?php echo e($section); ?>" class="container hero bg-img<?php echo e($custom_class); ?>">
    <section id="cpy-hero">
        <div class="content">
            <?php if($hero_title): ?>
                <div class="hero_intro_heading">
                    <?php echo $__env->make('partials.fc-content.home.hero-icons', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <h1><?php echo $hero_title; ?></h1>
                </div>
            <?php endif; ?>
            <?php echo $hero_description; ?>

            <?php if(get_sub_field('add_app_store_badges')): ?>
                <div class="mobile-apps-buttons">
                    <?php echo $__env->make('partials.components.global-mobile-apps', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            <?php endif; ?>
        </div>

        
        <?php if(!get_sub_field('add_app_store_badges') && !empty(get_sub_field('cta')['global_link_text'])): ?>
            <div class="cta-subtext-assurance">
                <div class="cta-subtext">
                    <?php if(get_sub_field('cta')): ?>
                        <?php echo e(get_sub_field('cta_subtext') ? $subtext = get_sub_field('cta_subtext') : $subtext = ''); ?>

                        <?php ($cta = get_sub_field('cta')); ?>
                        <?php echo $__env->make('partials.components.global-link', ['btn' => $cta, 'subtext' => $subtext, 'classes' => $include_slide_in_reg_form ? 'slide-in-form-btn' : ''], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        

        <?php if($include_reg_form): ?>
            <?php echo $__env->make('partials/fc-content/global/hero-signup-form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>

        <?php if($getapp): ?>
            <?php echo $__env->make('partials/fc-content/global/getapp', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
	</section>
</div>
