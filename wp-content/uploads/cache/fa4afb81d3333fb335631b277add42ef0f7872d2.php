<div class="container main-content">
	<section id="cpy-main-content">
    	<?php if(get_sub_field('title')): ?>
			<h2><?php echo e(get_sub_field('title')); ?></h2>
		<?php endif; ?>

		<?php echo get_sub_field('description'); ?>


    	<?php if(get_sub_field('image')): ?>
			<?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('image')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>

		<?php if(get_sub_field('features_exist') && get_sub_field('features')): ?>
			<div class="features-wrapper">
				<?php while(have_rows('features')): ?> <?php (the_row()); ?>
					<div class="feature">
						<?php if(get_sub_field('icon')): ?>
							<?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('icon')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						<?php endif; ?>
						<?php if(get_sub_field('icon_text')): ?>
							<div class="text-wrapper"><?php echo e(get_sub_field('icon_text')); ?></div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>


		<?php if(get_sub_field('include_cta')): ?>
			<?php if(get_sub_field('cta')): ?>
				<?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('cta'), 'classes' => 'ghost-button learn-more'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<?php endif; ?>
		<?php endif; ?>
	</section>
</div>
