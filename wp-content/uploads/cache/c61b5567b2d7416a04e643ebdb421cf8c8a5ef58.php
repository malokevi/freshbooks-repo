<div class="container" id="customer-testimonials">
        <section class="testimonials">
            <?php if(get_sub_field('section_title')): ?>
                <h2><?php echo e(get_sub_field('section_title')); ?></h2>
            <?php endif; ?>
            <?php if(have_rows('testimonials')): ?>
                <div class="smux-testimonials">
                    <?php while(have_rows('testimonials')): ?> <?php (the_row()); ?>
                        <div class="testimonial-block">
                            <div class="img-text">
                                <?php if(get_sub_field('photo')): ?>
                                    <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('photo'), 'classes' => 'content-img'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                <?php endif; ?>
                                <?php if(get_sub_field('testimonial')): ?>
                                    <em><q><?php echo e(get_sub_field('testimonial')); ?></q></em>
                                <?php endif; ?>
                            </div>
                            <hr>
                            <div class="bio">
                                <?php if(get_sub_field('name')): ?>
                                    <p><?php echo e(get_sub_field('name' )); ?></p>
                                <?php endif; ?>
                                <?php if(get_sub_field('title')): ?>
                                    <p class="subtext"><?php echo e(get_sub_field('title')); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </section>
    </div>
