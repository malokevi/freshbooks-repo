<?php
    // Page Values
    $custom_content = get_sub_field('custom_content');
    $title = get_sub_field('title') ? strtolower(preg_replace("/[^a-zA-Z]+/", "", get_sub_field('title'))) : 'cpy-cta-full-width';
    $postType = get_post_type();

    if ($postType === 'lpat_pages' || $postType === 'education') {
        $globalLinkAttr = [
            'btn' => [
                'global_link_url'    => get_field('trial_url') ?: get_field($postType === 'lpat_pages' ? 'try_it_free_cta_30_days' : 'education_centered_try_it_free_cta_30_days', 'options')['global_link_url'],
                'global_link_text'   => get_field('trial_cta_copy') ?: __('Try It Free', 'freshpress-theme'),
                'global_link_type'   => 'external',
                'global_link_or_cta' => 'cta',
                'global_cta_subtext' => '',
            ],
            'classes' => 'full-width-cta',
            'dataAttributes' => [
                'subscription-days' => get_field('trial_length') ?: 30
            ]
        ];
    } else {
        $globalLinkAttr = [
            'btn' => $custom_content ? get_sub_field('cta_btn') : get_field('cta_btn', 'option'),
            'classes' => isset($adobeInsightsExitLink) ? "full-width-cta $adobeInsightsExitLink" : 'full-width-cta',
        ];
    }
?>

<div class="container no-side-pad cta-full-wrap">
    <section class="cta-full-width" id="<?php echo e($title); ?>">
        <div class="two-col">
            <div class="col">
                <h2 class="cta-heading"><?php echo e($custom_content ? get_sub_field('cta_title') : get_field('cta_title', 'option')); ?></h2>
                <div class="cta-button">
                    <?php echo $__env->make('partials.components.global-link', $globalLinkAttr, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="cta-description">
                    <span><?php echo $custom_content ? get_sub_field('cta_subtext') : get_field('cta_subtext', 'option'); ?></span>
                </div>
            </div>
            <div class="col">
                <?php echo $__env->make('partials.components.global-image', ['img' => $custom_content ? get_sub_field('cta_image') : get_field('cta_image', 'option')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </section>
</div>
