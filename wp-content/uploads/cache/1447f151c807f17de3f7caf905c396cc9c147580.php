<?php
    if (!isset($menu)) {
        $menu = "primary_navigation";
    }
?>

<div class="desktop-nav">
    <div class="container">
        <a href="<?php echo e(home_url('/')); ?>" class="nav-logo">
            <img src="<?= App\asset_path('images/navigation/default-logo.svg'); ?>" alt="FreshBooks Cloud Accounting" width="120">
        </a>
        <?php echo $__env->make('partials.header.secondary-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="primary-nav">
            <div class="core">
                <div class="button-nav">
                    <?php if(get_field('sign_up_link', 'option')): ?>
                        <a class="top-level nav-item btn-primary goal-header-cta"
                           href="<?php echo e(get_field('sign_up_link', 'option')); ?>"><?php echo e(get_field('sign_up_link_text', 'option')); ?></a>
                    <?php endif; ?>

                    <?php if(get_field('login_link', 'option')): ?>
                        <a href="<?php echo e(get_field('login_link', 'option')); ?>"
                           class="top-level nav-item"><?php echo e(get_field('login_link_text', 'option')); ?></a>
                    <?php endif; ?>
                </div>
                <?php if(has_nav_menu($menu)): ?>
                    <?php echo wp_nav_menu(['theme_location' => $menu, 'container' => false, 'menu_class' => 'primary-nav-list', 'menu_id' => 'primary-nav', 'walker'          => new App\PrimaryNavWithDescription]); ?>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<div class="mobile-nav">
    <div class="mobile-nav-bar">
        <a href="<?php echo e(home_url('/')); ?>" class="nav-logo">
            <img src="<?= App\asset_path('images/navigation/default-logo.svg'); ?>" alt="FreshBooks Cloud Accounting" width="100">
        </a>
        <div class="mobile-buttons">

            <?php if(get_field('sign_up_link', 'option')): ?>
                <a href="<?php echo e(get_field('sign_up_link', 'option')); ?>" class="btn-primary">
                    <span><?php echo e(get_field('sign_up_link_text', 'option')); ?></span>
                </a>
            <?php endif; ?>

            <a href="javascript:;" class="menu-toggle item-toggle" data-target="responsive-menu">
                <img src="<?= App\asset_path('images/navigation/nav_hamburger.svg'); ?>" alt="Open Navigation" class="open-nav"
                     width="24" height="16">
                <img src="<?= App\asset_path('images/navigation/nav_close.svg'); ?>" alt="Close Navigation" class="close-nav">
            </a>
        </div>
    </div>

    <div class="responsive-menu">
        <?php if(has_nav_menu($menu)): ?>
            <?php echo wp_nav_menu(['theme_location' => $menu, 'container' => false, 'menu_class' => 'mobile_nav', 'menu_id' => '', 'link_before' => '<span>','link_after'=>'</span>', 'walker' => new App\PrimaryNavWithDescription]); ?>

        <?php endif; ?>

        <div class="nav-footer">
            <?php echo $__env->make('partials.header.secondary-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="secondary-buttons">

                <?php if(get_field('sign_up_link', 'option')): ?>
                    <a href="<?php echo e(get_field('sign_up_link', 'option')); ?>" class="btn-primary">
                        <span><?php echo e(get_field('sign_up_link_text', 'option')); ?></span>
                    </a>
                <?php endif; ?>

                <?php if(get_field('login_link', 'option')): ?>
                    <a href="<?php echo e(get_field('login_link', 'option')); ?>"
                       class="btn-ghost"><?php echo e(get_field('login_link_text', 'option')); ?></a>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
