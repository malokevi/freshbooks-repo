<?php
  $cta = get_sub_field('cta') || get_sub_field('cta_full_width_outline') || get_field('cta_full_width_outline')['cta'] ? true : false;
  $cta_text = get_sub_field('cta_outline_text') ?: get_sub_field('cta_full_width_outline')['text'] ?: get_field('cta_full_width_outline')['text'];
  $cta_btn = get_sub_field('cta') ?: get_sub_field('cta_full_width_outline')['cta'] ?: get_field('cta_full_width_outline')['cta'];
  if (!isset($adobeInsightsExitLink)) {
            $adobeInsightsExitLink = '';
    }
?>

<div class="container">
    <section id="cpy-feature-cta-1" class="full-width-cta-outline">
        <div class="outline-content">
            <?php echo $cta_text; ?>

            <?php if($cta): ?>
                <?php echo $__env->make('partials.components.global-link', ['btn' => $cta_btn], ['classes' => $adobeInsightsExitLink], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        </div>
    </section>
</div>
