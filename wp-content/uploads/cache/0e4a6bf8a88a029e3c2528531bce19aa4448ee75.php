<?php if(get_field('include_schedule_a_demo_buttons')): ?>
    <span class="scheduleDemoLink"><?php echo __('or', 'freshpress-theme'); ?> <a href="#"><?php echo __('Schedule a Demo', 'freshpress-theme'); ?></a></span>
<?php endif; ?>
