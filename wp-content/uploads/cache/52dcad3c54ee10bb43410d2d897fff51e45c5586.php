<?php
    $i18nEnabled = App\isI18nEnabled();
    $allRegions = App\getAllRegions();
    $hideCountrySelectionBanner = App\isCountryBannerHidden();
?>
<?php if($i18nEnabled && !empty($allRegions) && !$hideCountrySelectionBanner): ?>
    <?php
        $currentRegionLang = App\parseLanguageCode(App\getCurrentLanguage());
        $currentRegion = $allRegions[$currentRegionLang['country']][$currentRegionLang['lang']];
    ?>
    <div class="countrySelectionFooter region--<?php echo e(strtolower($currentRegionLang['country'])); ?>">
        <div class="countrySelectionFooter--input regionSelect">
            <span class="flag-icon flag-icon-<?php echo e(strtolower($currentRegionLang['country'])); ?>"></span>
            <div class="selector">
                <?php echo $currentRegion['countryName']; ?>

            </div>
        </div>
        <div class="countrySelectionFooter--modal modalRegion hidden">
            <?php $__currentLoopData = $allRegions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countryCode => $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                    $default = reset($languages); // Gets the first element from an assoc array
                    $multilingual = count($languages) > 1 ? 'multilingual' : '';
                ?>
                <a href="<?php echo $default['url']; ?>" class="region <?php echo e($multilingual); ?>" data-country-code="<?php echo e($countryCode); ?>" data-country-name="<?php echo e($default['countryName']); ?>">
                    <span class="flag-icon flag-icon-<?php echo e(strtolower($countryCode)); ?>"></span>
                    <?php echo e($default['countryName']); ?>

                </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="countrySelectionFooter--input languageSelect <?php echo e(count($allRegions[$currentRegionLang['country']]) === 1 ? 'hidden' : ''); ?>">
            <div class="selector">
                <?php echo $currentRegion['langName']; ?>

            </div>
        </div>
        <div class="countrySelectionFooter--modal modalLanguage hidden">
            <?php $__currentLoopData = $allRegions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countryCode => $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e($lang['url']); ?>" value="<?php echo e($lang['fullCode']); ?>" class="language <?php echo e(strtolower($countryCode)); ?>">
                        <?php echo e($lang['langName']); ?>

                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php endif; ?>
