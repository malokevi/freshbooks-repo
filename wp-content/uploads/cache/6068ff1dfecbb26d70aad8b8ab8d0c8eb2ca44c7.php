<?php
    $ctaModals = App\getCtaModalsCached();
?>

<?php if(!empty($ctaModals)): ?>
    <?php $__currentLoopData = $ctaModals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <?php
            $settings = $modal['modal_settings'];
            $content = $modal['modal_content'];
            $global = $settings['global'];
            $imageSegment = $content['image_segment'];
            $contentSegment = $content['content_segment'];
            $cta = $contentSegment['cta'];
            $bgColor = $imageSegment['background_color'] ?: '#062942';
            $bgImage = $imageSegment['background_image']['url'] ?: false;
            $triggerVar = $settings['trigger_method'] == 'scroll' ?
                'data-scroll-to="' . $settings['trigger_element'] . '"' :
                'data-time-delay="' . $settings['time_delay'] . '"';

            // Format title for use with cookies
            $title = sanitize_title_with_dashes(strtolower($settings['title']));

            // Set dismissed cookie if applicable
            $cookieVar = '';
            if ($settings['set_dismiss_cookie']) {
                $cookieVar = 'data-modal-dismiss="' . $settings['cookie_duration'] . '"';
            }

            // Check if this page shows the modal
            function inList($list) {
                return count(array_filter($list, function($val) {
                    global $post;
                    return isset($val->ID) && isset($post->ID) && $val->ID === $post->ID;
                }));
            }
            $showHere = (!$global && inList($settings['location'])) || ($global && !inList($settings['exceptions']));
        ?>

        <?php if($showHere): ?>
            <section class="ctaModalSection">
                <div id="<?php echo e($title); ?>" class="container ctaModal"
                     data-trigger="<?php echo e($settings['trigger_method']); ?>" <?php echo $triggerVar; ?> <?php echo $cookieVar; ?>>
                    <div class="ctaModal__modal">

                        <span class="ctaModal__popupDismiss btnDismiss">
                            <img src="<?= App\asset_path('images/icons/close.svg'); ?>" alt="close">
                        </span>

                        <div class="ctaModal__content">

                            <div class="ctaModal__segment imageSegment">
                                <?php if($imageSegment['foreground_image']['url']): ?>
                                    <img class="overlayImage" src="<?php echo e($imageSegment['foreground_image']['url']); ?>"
                                         alt="select">
                                <?php endif; ?>
                            </div>

                            <div class="ctaModal__segment copySegment">
                                <?php if($contentSegment['header']): ?>
                                    <h2><?php echo e($contentSegment['header']); ?></h2>
                                <?php endif; ?>

                                <?php if($contentSegment['body']): ?>
                                    <p><?php echo $contentSegment['body']; ?></p>
                                <?php endif; ?>

                                <div class="ctaModal__btns">
                                    <a href="<?php echo e($cta['url']['url']); ?>"
                                       class="btn <?php echo e($cta['style']); ?>"><?php echo e($cta['title']); ?></a>
                                    <a href="#"
                                       class="btnSecondary btnDismiss"><?php echo e($contentSegment['dismiss_anchor']); ?></a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

            <style>
                .imageSegment {
                    background-color: <?php echo e($bgColor); ?>;
                    background-image: url("<?php echo e($bgImage); ?>");
                }
            </style>

            <script>
                window.addEventListener('load', function () {
                    var modal = document.getElementById('<?php echo $title; ?>');
                    var dismissBtns = document.getElementsByClassName('btnDismiss');
                    var cookieName = 'modal-' + modal.id + '-dismissed';
                    var showState = readCookie(cookieName) && modal.dataset.modalDismiss ? 'dismissed' : 'not';

                    /**
                     * Use the set trigger method to display modal, if dismissal cookie not set
                     */
                    if (showState !== 'dismissed') {
                        if (modal.dataset.trigger === 'show') {
                            var delay = modal.dataset.timeDelay * 1000;
                            if (!isNaN(delay) && delay > 0) {
                                setTimeout(function () {
                                    modal.classList.add('ctaModal--visible');
                                }, delay);
                            } else {
                                modal.classList.add('ctaModal--visible');
                            }
                        } else if (modal.dataset.trigger === 'scroll') {
                            var section = modal.dataset.scrollTo;
                            if (section.length) {
                                var offset = jQuery(section).offset().top;
                                window.addEventListener('scroll', function () {
                                    var pos = window.pageYOffset || window.scrollY;
                                    if (pos > offset && showState === 'not') {
                                        modal.classList.add('ctaModal--visible');
                                        showState = 'shown';
                                    }
                                });
                            }
                        }
                    }

                    /**
                     * Bind modal dismissal to relevant buttons/anchors
                     */
                    Array.prototype.forEach.call(dismissBtns, function (dismissBtn) {
                        dismissBtn.addEventListener('click', function (e) {
                            e.preventDefault();
                            modal.classList.remove('ctaModal--visible');
                            if (modal.dataset.modalDismiss) {
                                createCookie(cookieName, 'true', modal.dataset.modalDismiss);
                            }
                        });
                    });
                });
            </script>
            <script>
                window.addEventListener('load', function () {
                    var submitBtns = document.getElementsByClassName('btn');
                    Array.prototype.forEach.call(submitBtns, function (submitBtn) {
                        submitBtn.addEventListener('click', function (e) {
                            window.dataLayer.push({
                                'event': 'ctaClick',
                                'ctaText': submitBtn.innerText,
                                'ctaSection': 'Modal',
                            });
                        });
                    });
                });
            </script>
        <?php endif; ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
