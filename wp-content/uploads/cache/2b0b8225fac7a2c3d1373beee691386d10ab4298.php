<section class="threeColFeatures">
    <h3 class="threeColFeatures__heading text-center"><?php echo e(get_sub_field('section_heading')); ?></h3>
    <div class="threeColFeatures__features">
        <?php $__currentLoopData = get_sub_field('features'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="threeColFeatures__feature">
                <div class="threeColFeatures__imageWrapper">
                    <div class="threeColFeatures__imageInner">
                        <img src="<?php echo e($feature['image']); ?>" alt="<?php echo e($feature['heading']); ?>" />
                    </div>
                </div>
                <h4><?php echo e($feature['heading']); ?></h4>
                <?php echo $feature['description']; ?>

            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div class="threeColFeatures__ctaContainer text-center">
        <?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('cta')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</section>
