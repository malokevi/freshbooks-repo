<?php if(get_sub_field('image_fw')): ?>
    <div class="container no-side-pad fresh-faces lazy" data-src="<?php echo e(get_sub_field('image_fw')['url']); ?>"></div>
<?php endif; ?>