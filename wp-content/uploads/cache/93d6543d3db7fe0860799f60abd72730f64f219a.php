<?php if(!get_field('override_wp_footer')): ?>
    <?php
        $page_template = basename(get_page_template());
        $is_classic = $page_template === 'addons.blade.php' || App\is_post_type('addons') || is_tax('addons_categories') || $page_template === 'benefits.blade.php' || $page_template === 'business-name-generator.blade.php';
        $hasStickyCTA = get_field('sticky-cta_active', 'option');
    ?>
    <footer class="<?php echo e($hasStickyCTA ? 'sticky-cta' : ''); ?>">
        <?php if(
            !get_field('remove_footer_menu') &&
            !App\is_post_type('accounting_templates') &&
            !App\is_post_type('invoice_templates') &&
            !is_tax('invoice_templates_categories') &&
            !App\is_post_type('accounting_software') &&
            !is_tax('accounting_software_categories') &&
            !$is_classic &&
            !App\is_post_type('lpat_pages') &&
            !App\is_post_type('education') &&
            !App\is_post_type('as_v2_cpt') &&
            !is_tax('as_v2_categories') &&
            $page_template !== 'brand-campaign.blade.php' &&
            $page_template !== 'info-hub-page.blade.php'
        ): ?>
            <?php echo $__env->make('partials.components.global-footer-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>

        <?php if(!$is_classic): ?>
            <?php echo $__env->make('partials.fc-content.global.divider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('partials.components.global-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php else: ?>
            <?php echo $__env->make('partials.components.global-classic-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    </footer>
<?php endif; ?>
