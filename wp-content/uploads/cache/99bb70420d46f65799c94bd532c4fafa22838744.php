<?php
    $page_template = basename(get_page_template());
    $post_id = 'option';
    $post_type = get_post_type();
    $adobeInsightsExitLink = isset($adobeInsightsExitLink) ? $adobeInsightsExitLink : '';

    if ($post_type === 'lpat_pages' || $post_type === 'education') {
        $trial_length = get_field('trial_length') ?: 30;
        $trial_cta_copy = get_field('trial_cta_copy') ?: __('Try It Free', 'freshpress-theme');
        $trial_url = get_field('trial_url') ?: get_field($post_type === 'lpat_pages' ? 'try_it_free_cta_30_days' : 'education_try_it_free_cta_30_days', 'options')['global_link_url'];
    }

    if ($page_template === 'flexible-acquisition-page.blade.php') {
        $trial_length = 30;
        $trial_cta_copy = get_sub_field('trial_cta')['global_link_text'];
        $trial_url = get_sub_field('trial_cta')['global_link_url'];
    }
?>

<?php if(get_sub_field('use_custom_slides')): ?>
    <?php ($post_id = $post->ID); ?>
<?php endif; ?>

<?php if(have_rows('carousel_slides', $post_id)): ?>
    <div class="container no-side-pad contentCarousel">
        <section id="cpy-carousel">
            <a class="anchor" <?php echo e(isset($counter) ? "id=\"section$counter\"" : ''); ?>></a>
            <div class="container blue-background carousel-nav">
                <section>
                    <div class="features-nav">
                        <?php while(have_rows('carousel_slides', $post_id)): ?> <?php (the_row()); ?>
                            <a href="#" class="tab <?php echo e(get_row_index() == 1 ? 'active' : ''); ?>" data-target="<?php echo e(get_row_index()); ?>"><?php echo e(get_sub_field('carousel_slide_label', $post_id)); ?></a>
                        <?php endwhile; ?>
                    </div>
                </section>
            </div>
            <div class="blue-white-background container">
                <div class="feature-arrow left-arrow">
                    <img src="<?= App\asset_path('images/carousel/arrow-left.svg'); ?>" alt="<?php echo e(__('Previous Feature', 'freshpress-theme')); ?>">
                </div>
                <section class="features">
                    <?php while(have_rows('carousel_slides', $post_id)): ?> <?php (the_row()); ?>
                        <div class="slide" data-target="<?php echo e(get_row_index()); ?>">
                            <div class="feature invoicing">
                                <div class="feature-content">
                                    <?php if(get_sub_field('carousel_slide_title', $post_id)): ?>
                                        <h3><?php echo e(get_sub_field('carousel_slide_title', $post_id)); ?></h3>
                                    <?php endif; ?>

                                    <?php if(get_sub_field('carousel_slide_content', $post_id)): ?>
                                        <p><?php echo e(get_sub_field('carousel_slide_content', $post_id)); ?></p>
                                    <?php endif; ?>

                                    <?php if(get_sub_field('carousel_slide_cta', $post_id)): ?>
                                        <?php if($page_template === 'flexible-acquisition-page.blade.php'): ?>
                                            <a href="<?php echo e($trial_url); ?>" class="primary-cta lpat-carousel-cta" target="_blank" rel="noopener" data-subscription-days="<?php echo e($trial_length); ?>"><?php echo e($trial_cta_copy); ?></a>
                                        <?php elseif(
                                            ($page_template != 'home-page.blade.php' && !get_sub_field('carousel_sign_up_link', $post_id))
                                            && $post_type !== 'lpat_pages'
                                            && $post_type !== 'education'
                                            && $page_template !== 'brand-campaign.blade.php'
                                            && ($page_template !== 'partner-page.blade.php' || get_field('carousel_sign_up_link', $post_id))
                                        ): ?>
                                            <?php echo $__env->make('partials.components.global-link', ['btn' => get_field('carousel_sign_up_link', $post_id), 'classes' => $adobeInsightsExitLink], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php elseif($page_template === 'partner-page.blade.php'): ?>
                                            <?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('carousel_slide_cta', $post_id), 'classes' => $adobeInsightsExitLink], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php elseif($post_type === 'lpat_pages' || $post_type === 'education'): ?>
                                            <a href="<?php echo e($trial_url); ?>" class="primary-cta lpat-carousel-cta" target="_blank" rel="noopener" data-subscription-days="<?php echo e($trial_length); ?>"><?php echo e($trial_cta_copy); ?></a>
                                        <?php elseif($page_template === 'brand-campaign.blade.php'): ?>
                                            <?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('carousel_slide_cta', $post_id), 'classes' => 'cta cta--green'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php else: ?>
                                            <?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('carousel_slide_cta', $post_id), 'classes' => 'ghost-button no-width info-button'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>

                                <div class="feature-image">
                                    <?php if(get_sub_field('carousel_slide_image', $post_id)): ?>
                                        <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('carousel_slide_image', $post_id)], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>
                    <?php endwhile; ?>
                </section>
                <div class="feature-arrow right-arrow">
                    <img src="<?= App\asset_path('images/carousel/arrow-right.svg'); ?>" alt="<?php echo e(__('Next Feature', 'freshpress-theme')); ?>">
                </div>
                <?php if($post_type==='lpat_pages'): ?>
                    <div class="centered-cta-mobile">
                        <?php echo $__env->make('partials.components.cta.centered-cta-mobile', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    </div>
<?php endif; ?>
