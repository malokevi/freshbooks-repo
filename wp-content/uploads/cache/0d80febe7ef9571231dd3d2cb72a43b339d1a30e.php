<div class="featuredNotice">
    <?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('image'), 'classes' => 'featuredNotice__image'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="featuredNotice__copy">
        <?php echo get_sub_field('copy'); ?>

    </div>
</div>
