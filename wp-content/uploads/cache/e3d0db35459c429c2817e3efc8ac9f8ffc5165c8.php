<?php
    $popups = App\getCtaPopupsCached();
    $loadAssets = false;
?>
<?php if(!empty($popups)): ?>
    <?php $__currentLoopData = $popups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $popup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $settings = $popup['popup_settings'];
            $content = $popup['popup_content'];
            $active = componentIsActive($settings, get_queried_object_id());
            $loadAssets = $active || $loadAssets;
            $popupId = 'cta-popup-' . sanitize_title_with_dashes(strtolower($settings['popup_id']));
            $dataAttrVals = App\data_attributes($settings['data_attributes']);
            $nfbCustomerVisibility = 'data-nfb-visibility=' . $settings['nfb_customer_visibility'];
        ?>

        <?php if($active): ?>
            <style media="screen">
                #<?php echo e($popupId); ?> .ctaPopup__content {
                    background-color: <?php echo e($content['background_color']); ?>;
                }

                #<?php echo e($popupId); ?> .ctaPopup__header h2 {
                     color: <?php echo e($content['font_color']); ?>;
                 }

                <?php if (!empty($content['cta_side_image']['global_image']['url']) && $content['enable_cta_side_image']) : ?>
                    #<?php echo e($popupId); ?> .cta-after-image::after {
                        background-image: url("<?php echo $content['cta_side_image']['global_image']['url'] ?: ''; ?>");
                    }
                <?php endif; ?>

                <?php if (!empty($content['cta_side_image_mobile']['global_image']['url']) && $content['enable_cta_side_image']) : ?>
                    @media  screen and (max-width: 767px) {
                        #<?php echo e($popupId); ?> .cta-after-image::after {
                            background-image: url("<?php echo $content['cta_side_image_mobile']['global_image']['url']; ?>");
                        }
                    }
                <?php endif; ?>
            </style>

            <div id="<?php echo e($popupId); ?>" class="ctaPopup hidden" data-gtm-promo-name="<?php echo e($popupId); ?>" data-delay-length="<?php echo e($settings['popup_delay']); ?>" <?php echo $dataAttrVals; ?> <?php echo $nfbCustomerVisibility; ?>>
                <div class="ctaPopup__content">

                    <span class="ctaPopup__popupDismiss popupDismiss">
                        <?php if($content['white_exit_icon']): ?>
                            <img src="<?= App\asset_path('images/icons/close-thick.svg'); ?>" alt="close">
                        <?php else: ?>
                            <img src="<?= App\asset_path('images/icons/close-dark.svg'); ?>" alt="close">
                        <?php endif; ?>
                    </span>

                    <div class="ctaPopup__header">
                        <h2><?php echo $content['header']; ?></h2>
                        <?php echo $content['body']; ?>

                    </div>

                    <?php if($content['animated_image']): ?>
                        <div class="ctaPopup__animation">
                            <img class="ctaPopup__animation--image" src="<?php echo e($content['popup_animation']['placeholder']['url']); ?>" alt="popup-image" data-img-src="<?php echo e($content['popup_animation']['animation']['url']); ?>">
                        </div>
                    <?php else: ?>
                        <div class="ctaPopup__image">
                            <?php echo $__env->make('partials.components.global-image', ['img' => $content['popup_image']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    <?php endif; ?>

                    <div class="ctaPopup__cta">
                        <?php echo $__env->make('partials.components.global-link', ['btn' => $content['cta'], 'classes' => 'cta-after-image'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>

                    <?php echo do_shortcode('[global_countdown id="fall-sale-timer" class="popupTimer"]'); ?>

                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php if($loadAssets): ?>
        <?php
            wp_enqueue_style('cta-popups', App\asset_path('styles/cta-popups.css'), false, null);
            wp_enqueue_script('cta-popups', App\asset_path('scripts/cta-popups.js'), ['jquery'], null, true);
        ?>
    <?php endif; ?>
<?php endif; ?>
