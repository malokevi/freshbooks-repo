<?php
	// Page Values
	$id = get_sub_field('title') ? sanitize_title_with_dashes(strtolower(get_sub_field('title'))) : 'cpy-cta-full-width';
	$right_img = get_sub_field('right_side_image');
?>

<div class="container no-side-pad cta-full-wrap">
	<section class="ctaLander__fullWidth-3" id="<?php echo e($id); ?>">
		<div class="three-col">
			<div class="col">
				<?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('left_side_image'), 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
			<div class="col">
					<h2 class="cta__heading"><?php echo strip_tags(get_sub_field( 'title' ), '<br>'); ?><span class="cta__subheading"><?php echo get_sub_field('subtitle') ? get_sub_field('subtitle') : ''; ?></span></h2>
				<div class="cta__description">
					<?php echo get_sub_field('caption') ? get_sub_field('caption') : get_field('cta_subtext', 'option'); ?>

				</div>
				<div class="cta__button">
						<a href="<?php echo e(get_field('signup_url')); ?>" class="primary-cta full-width-cta" rel="noopener"><?php echo e(get_sub_field('button_text') ? get_sub_field('button_text') : get_field('cta_title', 'option')); ?></a>
				</div>
			</div>
		</div>
	</section>
</div>
