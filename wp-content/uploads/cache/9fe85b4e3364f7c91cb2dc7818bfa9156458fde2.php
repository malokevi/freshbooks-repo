<?php
    $plans = get_sub_field('pricing_plans');

    $regularPlansImg['global_image'] = get_sub_field('payed_plans_topper');

    $promo = false;
                    
    if( have_rows('tabs') ) {
        $tab_titles = [];

        while( have_rows('tabs') ) {
            the_row();
            $tab_titles[] = get_sub_field('tab_title');
        }
    }

    while( have_rows('pricing_plans') ) {
        the_row();

        $promo = get_sub_field('promo_mode');

        while(have_rows('free_trial')) {
            the_row();      
            $plan_costs['free_trial'] = 0;
        }

        while(have_rows('plans')) {
            the_row();

            if (get_sub_field('yearly_cost')) {
                //pricing plan costs
                $cost_yr = $promo ? get_sub_field('promo_yearly_cost') : get_sub_field('yearly_cost');
                $cost_yr_reg = get_sub_field('yearly_cost');

                $plan_costs[get_sub_field('plan_name')]['year'] = $cost_yr;
                $plan_costs[get_sub_field('plan_name')]['year_reg'] = $cost_yr_reg;
            }

            if (get_sub_field('monthly_cost')) {
                $cost_mo_reg = get_sub_field('monthly_cost');
                $cost_mo = $promo ? get_sub_field('promo_monthly_cost') : get_sub_field('monthly_cost');      

                $plan_costs[get_sub_field('plan_name')]['month'] = $cost_mo;
                $plan_costs[get_sub_field('plan_name')]['month_reg'] = $cost_mo_reg;
            }
        }

        if (get_sub_field('promo_mode')) {
            $start = DateTime::createFromFormat('Y-m-d H:i:s', get_sub_field('promo_duration')['start'], new DateTimeZone('America/Toronto'));
            $end = DateTime::createFromFormat('Y-m-d H:i:s', get_sub_field('promo_duration')['end'], new DateTimeZone('America/Toronto'));
            $now = new DateTime("now", new DateTimeZone('America/Toronto'));
            
            if ( ($now > $start && $now < $end) || (empty($start) && empty($end)) ) {
                $promo = true;

                while(have_rows('promo_icons')) {
                    the_row();
                    $promo_month_icon = get_sub_field('promo_month_icon');
                    $promo_year_icon = get_sub_field('promo_year_icon');
                }
            }
        }
    }

    $showYearly = get_sub_field('show_yearly');
?>

<style media="screen">
  .switch-side-image::before {
    background-image: url("<?php echo e(get_sub_field('right_tab_image')); ?>");
  }
</style>

<div id="pricing-plans" class="container pricing-table-columns">
    <section>
        <?php if(strlen(get_sub_field('pricing_plans_header')) > 0): ?>
            <h2><?php echo e(get_sub_field('pricing_plans_header')); ?></h2>
        <?php endif; ?>
        
        <?php if( get_sub_field('pricing_plans') ): ?>
            <div class="currency-disclaimer">
                <?php echo e(__('All Prices in USD', 'freshpress-theme')); ?>

            </div>
            
            <div class="pricing-tiers-v2">
            <?php while( have_rows('pricing_plans') ): ?>
                    <?php
                        the_row();
                    ?>

                <?php if( have_rows('features_column') ): ?>
                    <?php while(have_rows('features_column')): ?>
                        <?php
                            the_row();
                        ?>

                        <div class="pricing-tier pricing-tier--features">
                            <div class="plan-header">
                                <div class="features-title">
                                    <?php if(strlen(get_sub_field('features_heading')) > 0): ?>
                                        <?php if(get_sub_field('features_icon')): ?>
                                            <?php
                                                $features_icon['global_image']['url'] = get_sub_field('features_icon');
                                            ?>
                                            <?php echo $__env->make('partials.components.global-image', ['img' => $features_icon, 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php endif; ?>
                                        <?php if(get_sub_field('features_heading')): ?>
                                            <h3 class="plan-name"><?php echo e(get_sub_field('features_heading')); ?></h3>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                 </div>
                            </div>
 
                            <div class="plan-details">
                                <div class="features-mobile features-mobile--headings">
                                    <span>Try It Free</span>
                                    <span>Lite</span>
                                    <span>Plus</span>
                                    <span>Premium</span>
                                </div>

                                <?php 
                                    $count = 0;
                                    $seemore_padfix_classes = [];
                                ?>
                                <?php while(have_rows('features_repeater')): ?>
                                    <?php
                                        the_row();

                                        if(get_sub_field('auto_open') && get_sub_field('auto_open') == true)   {
                                            $auto_open = "true";
                                        } else {
                                            $auto_open = "false";
                                        }

                                        $seemore_classes = '';

                                        if(null !== get_sub_field('feature_subcategories') && count(get_sub_field('feature_subcategories')) <= 2) {
                                            $seemore_classes = 'see__more--hide';
                                            $seemore_padfix_classes[] = $count;
                                        }
                                    ?>

                                    <div class="plan-feature-block <?php echo e($seemore_classes); ?>" aria-open="<?php echo e($auto_open); ?>" aria-carousel-id="slider-<?php echo e($count); ?>">
                                        <?php if(get_sub_field('feature_category')): ?>
                                            <h4 class="plan-name"><?php echo e(get_sub_field('feature_category')); ?></h4>
                                        <?php endif; ?>

                                        <?php while(have_rows('feature_subcategories')): ?>
                                            <?php
                                                the_row();
                                            ?>

                                            <?php if(get_sub_field('subcategory_label')): ?>
                                                <?php if(get_sub_field('tooltip')): ?>
                                                    <p><?php echo get_sub_field('subcategory_label', true); ?> 
                                                        <span class="feature-tooltip">
                                                            <img src="<?= App\asset_path('images/icons/tooltip.svg'); ?>">
                                                        </span>
                                                    </p>
                                                    <ul class="plan-subfeatures">
                                                        <li><?php echo get_sub_field('tooltip'); ?></li>
                                                    </ul>
                                                <?php else: ?>
                                                    <p><?php echo get_sub_field('subcategory_label', true); ?></p>
                                                <?php endif; ?>

                                                <?php
                                                    $free = false;
                                                    $lite = false;
                                                    $plus = false;
                                                    $premium = false;
                                                ?>

                                                <?php if(is_array(get_sub_field('active_plans'))): ?>
                                                    <?php $__currentLoopData = get_sub_field('active_plans'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ap): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                                     
                                                        <?php if(strtolower($ap['value']) == 'free trial'): ?> 
                                                            <?php
                                                                $free = true;
                                                            ?>
                                                        <?php elseif(strtolower($ap['value']) == 'lite'): ?>
                                                            <?php    
                                                                $lite = true;
                                                            ?>
                                                        <?php elseif(strtolower($ap['value']) == 'plus'): ?>
                                                            <?php    
                                                                $plus = true;
                                                            ?>
                                                        <?php elseif(strtolower($ap['value']) == 'premium'): ?>
                                                            <?php    
                                                                $premium = true;
                                                            ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>

                                                <div class="features-mobile features-mobile--checkmarks">
                                                    <?php if($free == true): ?>   
                                                        <span>
                                                            <img src="<?= App\asset_path('images/icons/checkmark-white.svg'); ?>">
                                                        </span>
                                                    <?php else: ?>
                                                        <span></span>
                                                    <?php endif; ?>

                                                    <?php if($lite == true): ?>
                                                        <span>
                                                            <img src="<?= App\asset_path('images/icons/checkmark-blue.svg'); ?>">
                                                        </span>
                                                    <?php else: ?>
                                                        <span></span>
                                                    <?php endif; ?>

                                                    <?php if($plus == true): ?>
                                                        <span>
                                                            <img src="<?= App\asset_path('images/icons/checkmark-blue.svg'); ?>">
                                                        </span>
                                                    <?php else: ?>
                                                        <span></span>
                                                    <?php endif; ?>

                                                    <?php if($premium == true): ?>
                                                        <span>
                                                            <img src="<?= App\asset_path('images/icons/checkmark-blue.svg'); ?>">
                                                        </span>
                                                    <?php else: ?>
                                                        <span></span>
                                                    <?php endif; ?>                                                    
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>

                                        <a href="#" class="see__more"><span class="less-more">more</span> <span class="caret">^</span></a>
                                    </div>

                                    <?php 
                                        $count += 1;
                                    ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            <?php endwhile; ?>

                <?php while( have_rows('pricing_plans') ): ?>
                    <?php
                        the_row();
                    ?>

                    <?php while(have_rows('free_trial')): ?>
                        <?php
                            the_row();
                            $free_trial_topper['global_image'] = get_sub_field('free_trial_topper');
                        ?>
                        <div class="pricing-tier pricing-tier--free">                        
                            <?php echo $__env->make('partials.components.global-image', ['img' => $free_trial_topper, 'classes' => 'icon-most-popular', 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                            <div class="floating-label-space">
                                <div class="floating-label">
                                    <?php if(get_sub_field('plan_name')): ?>
                                        <h3 class="plan-name"><?php echo e(get_sub_field('plan_name')); ?></h3>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="plan-header">
                                <?php if(get_sub_field('plan_name')): ?>
                                    <h3 class="plan-name"><?php echo e(get_sub_field('plan_name')); ?></h3>
                                <?php endif; ?>

                                <div class="plan-values">
                                    
                                    <?php  ?>
                                    <?php if($plan_costs['free_trial'] == '0'): ?>
                                        <?php
                                            $cost_mo = $plan_costs['free_trial'];
                                            $mo_numeric = is_numeric($cost_mo);
                                            if (strpos($cost_mo, '.')) {
                                                $cost_split = explode('.', $cost_mo);
                                                $dollars = $cost_split[0];
                                                $cents = '<sup class="cents">.' . $cost_split[1] . '</sup>';
                                            } else {
                                                $dollars = trim($cost_mo);
                                                $cents = '<sup class="cents">.00</sup>';
                                            }
                                        ?>
                                        <div class="subscription-type type-one type-two show-plan">
                                            <h3 class="plan-cost <?php echo e(!$mo_numeric ? 'cost-text' : ''); ?>">
                                                <?php if($mo_numeric): ?>
                                                    <sup>$</sup>
                                                <?php endif; ?>
                                                <?php echo $dollars; ?><?php echo $cents; ?>

                                                <?php if($mo_numeric): ?>
                                                    <sub class='<?php echo e(!empty($cents) ? "with-cents" : ""); ?>'>/30 days</sub>
                                                <?php endif; ?>
                                            </h3>
                                        </div>
                                    <?php endif; ?>

                                    <?php 
                                        $global_link_text['global_link_text'] = get_sub_field('link_text');
                                        $global_link_text['global_link_url'] = get_field('signup_url');
                                    ?>

                                    <?php if($global_link_text): ?>
                                        <?php echo $__env->make('partials.components.global-link', ['btn' => $global_link_text, 'classes' => 'pricing-btn primary-cta'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                    <?php endwhile; ?>
                            <div class="plan-details">
                        <?php while(have_rows('features_column')): ?>        
                            <?php
                                the_row();
                                $count = 0;
                            ?>
                                    
                                <?php while(have_rows('features_repeater')): ?>
                                    <?php
                                        the_row();
                                    ?>

                                    <div class="plan-checkmarks <?php echo e(in_array($count, $seemore_padfix_classes) ? 'plan-checkmarks--padfix' : ''); ?>" aria-carousel-id="slider-<?php echo e($count); ?>">
                                        
                                        <div class="checkmark-label"></div>
                                        
                                        <?php while(have_rows('feature_subcategories')): ?>
                                            <?php
                                                the_row();
                                            ?>

                                           <?php if(is_array(get_sub_field('active_plans'))): ?>                                                
                                                <?php
                                                    $plan_active = false;

                                                    foreach(get_sub_field('active_plans') as $active) {
                                                        if(strtolower($active['value']) == 'free trial') {
                                                            $plan_active = true;
                                                            break;
                                                        }
                                                    }
                                                ?>

                                                <?php if($plan_active == true): ?>
                                                    <span class="plan-name"><img src="<?= App\asset_path('images/icons/checkmark-white.svg'); ?>" alt=""></span>
                                                <?php else: ?> 
                                                    <span class="plan-name"></span>
                                                <?php endif; ?>
                                            <?php else: ?> 
                                                <span class="plan-name"></span>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php
                                        $count += 1;
                                    ?>
                                <?php endwhile; ?>
                                <?php if(!empty($seemore_padfix_classes)): ?> 
                                    <div class="checkmark__bumper"></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endwhile; ?>

                <?php while( have_rows('pricing_plans') ): ?>
                    <?php
                        the_row();
                    ?>
                    
                    <?php while(have_rows('features_column')): ?>        
                        <?php
                            the_row();
                            $dataRow = get_row(); 
                        
                            foreach ($dataRow as $key => $value) {                
                                if ( !empty($value) ) {
                                    $field = get_sub_field_object( $key ); 
                                    if($field['_name'] == 'features_repeater') {
                                        $dataRow = $field;
                                    }
                                }
                            }

                            $repeaterData = $dataRow['value'];
                        ?>
                    <?php endwhile; ?>

                    <?php 
                        $counter = 0;
                    ?>
                    <?php while(have_rows('plans')): ?>
                        <?php
                            the_row();
                        ?>
                        <div class="pricing-tier pricing-tier--paid">
                            <?php if($promo && !$promo_exclusion && $promo_month_icon['url'] && $counter == 1): ?>
                                <img src="<?php echo e($promo_month_icon['url']); ?>" alt="<?php echo e($promo_month_icon['alt']); ?>" class="promo-icon type-one subscription-type mobile-promo-icon icon-most-popular icon-pay-as-you-grow <?php echo e($showYearly ? '' : 'show-plan'); ?>">
                            <?php endif; ?>
                            <?php if($promo && !$promo_exclusion && $promo_year_icon['url'] && $counter == 1): ?>
                                <img src="<?php echo e($promo_year_icon['url']); ?>" alt="<?php echo e($promo_year_icon['alt']); ?>" class="promo-icon type-two subscription-type mobile-promo-icon icon-most-popular icon-pay-as-you-grow <?php echo e($showYearly ? 'show-plan' : ''); ?>">
                            <?php endif; ?>
                            <?php if($promo && !$promo_exclusion && $promo_month_icon['url'] && $counter == 1): ?>
                                <img src="<?php echo e($promo_month_icon['url']); ?>" alt="<?php echo e($promo_month_icon['alt']); ?>" class="promo-icon type-one subscription-type icon-most-popular icon-pay-as-you-grow <?php echo e($showYearly ? '' : 'show-plan'); ?>">
                            <?php endif; ?>
                            <?php if($promo && !$promo_exclusion && $promo_year_icon['url'] && $counter == 1): ?>
                                <img src="<?php echo e($promo_year_icon['url']); ?>" alt="<?php echo e($promo_year_icon['alt']); ?>" class="promo-icon type-two subscription-type icon-most-popular icon-pay-as-you-grow <?php echo e($showYearly ? 'show-plan' : ''); ?>">
                            <?php endif; ?>

                            <div class="floating-label-space">
                                <div class="floating-label">
                                    <?php if(get_sub_field('plan_name')): ?>
                                        <h3 class="plan-name"><?php echo e(get_sub_field('plan_name')); ?></h3>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="plan-header<?php echo e($counter == 0 ? ' first-plan' : ''); ?>">
                                <?php if(get_sub_field('plan_name')): ?>
                                    <h3 class="plan-name <?php echo e($counter == 1 && $promo == true ? 'plan-name--fixpad' : ''); ?>"><?php echo e(get_sub_field('plan_name')); ?></h3>
                                <?php endif; ?>

                                <div class="plan-values">
                                    <?php if(isset($plan_costs[get_sub_field('plan_name')]['month'])): ?>
                                        <?php
                                            $cost_mo = $plan_costs[get_sub_field('plan_name')]['month']; 
                                            $mo_numeric = is_numeric($cost_mo);
                                            if (strpos($cost_mo, '.')) {
                                                $cost_split = explode('.', $cost_mo);
                                                $dollars = $cost_split[0];
                                                $cents = '<sup class="cents">.' . $cost_split[1] . '</sup>';
                                            } else {
                                                $dollars = trim($cost_mo);
                                                $cents = '<sup class="cents">.00</sup>';
                                            }

                                            $cost_mo_reg = $plan_costs[get_sub_field('plan_name')]['month_reg'];
                                            $mo_reg_numeric = is_numeric($cost_mo_reg);
                                            if (strpos($cost_mo_reg, '.')) {
                                                $cost_split = explode('.', $cost_mo_reg);
                                                $dollars_reg = $cost_split[0];
                                                $cents_reg = '<sup class="cents">.' . $cost_split[1] . '</sup>';
                                            } else {
                                                $dollars_reg = trim($cost_mo_reg);
                                                $cents_reg = '<sup class="cents">.00</sup>';
                                            }
                                        ?>
                                        <div class="subscription-type type-one <?php echo e($showYearly ? '' : 'show-plan'); ?>">
                                            <h3 class="plan-cost <?php echo e(!$mo_numeric ? 'cost-text' : ''); ?>">
                                                <?php if($mo_numeric): ?>
                                                    <sup>$</sup>
                                                <?php endif; ?>
                                                <?php echo $dollars; ?><?php echo $cents; ?>

                                                <?php if($mo_numeric): ?>
                                                    <sub class='<?php echo e(!empty($cents) ? "with-cents" : ""); ?>'>/mo</sub>
                                                <?php endif; ?>
                                            </h3>

                                            <?php if($promo): ?>
                                                <div class="reg-cost">
                                                    <?php if(is_numeric($cost_mo_reg)): ?>
                                                        REG. <span class="strikethrough">$<?php echo $dollars_reg; ?><?php echo $cents_reg; ?></span>
                                                    <?php else: ?>
                                                        &nbsp;
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if(isset($plan_costs[get_sub_field('plan_name')]['year'])): ?>
                                        <?php
                                            $cost_yr = $plan_costs[get_sub_field('plan_name')]['year'];
                                            $yr_numeric = is_numeric($cost_yr);
                                            if (strpos($cost_yr, '.')) {
                                                $cost_split = explode('.', $cost_yr);
                                                $dollars = $cost_split[0];
                                                $cents = '<sup class="cents">.' . $cost_split[1] . '</sup>';
                                            } else {
                                                $dollars = trim($cost_yr);
                                                $cents = '<sup class="cents">.00</sup>';
                                            }

                                            $cost_yr_reg = $plan_costs[get_sub_field('plan_name')]['year_reg'];
                                            $yr_reg_numeric = is_numeric($cost_yr_reg);
                                            if (strpos($cost_yr_reg, '.')) {
                                                $cost_split = explode('.', $cost_yr_reg);
                                                $dollars_reg = $cost_split[0];
                                                $cents_reg = '<sup class="cents">.' . $cost_split[1] . '</sup>';
                                            } else {
                                                $dollars_reg = trim($cost_yr_reg);
                                                $cents_reg = '<sup class="cents">.00</sup>';
                                            }
                                        ?>
                                        <div class="subscription-type type-two <?php echo e($showYearly ? 'show-plan' : ''); ?>">
                                            <h3 class="plan-cost <?php echo e(!$yr_numeric ? 'cost-text' : ''); ?>">
                                                <?php if($yr_numeric): ?>
                                                    <sup>$</sup>
                                                <?php endif; ?>
                                                <?php echo $dollars; ?><?php echo $cents; ?>

                                                <?php if($yr_numeric): ?>
                                                    <sub class="<?php echo e(!empty($cents) ? "with-cents" : ""); ?>">/mo</sub>
                                                <?php endif; ?>
                                            </h3>

                                            <?php if($promo): ?>
                                                <div class="reg-cost">
                                                    <?php if(is_numeric($cost_yr)): ?>
                                                        REG. <span class="strikethrough">$<?php echo $dollars_reg; ?><?php echo $cents_reg; ?></span>
                                                    <?php else: ?>
                                                        &nbsp;
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php
                                        $buttonText = get_sub_field('link_text');
                                        $buttonArray['cta']['global_page_link'] = $buttonText;
                                    ?>

                                    <?php if(isset($buttonArray['cta']) && is_array($buttonArray['cta']) && strlen($buttonArray['cta']['global_link_text']) > 0): ?>
                                        <?php echo $__env->make('partials.components.global-link', ['btn' => $buttonArray['cta'], 'classes' => 'pricing-link'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>
                                </div>
                                 
                                <?php if($counter == 2 && isset($tab_titles)): ?>
                                    <div class="container pricing-table-tabs" id="pricing-table-tabs">
                                        <section>
                                            <div class="subscriptions-toggle column-parent">
                                                <div class="switch-container switch-side-image">
                                                    <span class="label type-one-label <?php echo e($showYearly ? '' : 'active'); ?>"><?php echo $tab_titles[0]; ?></span>
                                                    <label class="switch">
                                                        <input type="checkbox" <?php echo e($showYearly ? 'checked' : ''); ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <span class="label type-two-label <?php echo e($showYearly ? 'active' : ''); ?>"><?php echo $tab_titles[1]; ?></span>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                <?php endif; ?>
                            </div>
                                                
                            <div class="plan-details">
                                <?php    
                                    $count = 0;
                                ?>
                                <?php $__currentLoopData = $repeaterData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        if(isset($dr['auto_open']) && $dr['auto_open'] == true) {
                                            $auto_open = "true";
                                        } else {
                                            $auto_open = "false";
                                        }
                                    ?>

                                    <div class="plan-checkmarks <?php echo e(in_array($count, $seemore_padfix_classes) ? 'plan-checkmarks--padfix' : ''); ?>" aria-open="<?php echo e($auto_open); ?>" aria-carousel-id="slider-<?php echo e($count); ?>">

                                        <div class="checkmark-label"></div>
                                        <?php $__currentLoopData = $dr['feature_subcategories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(is_array($sc['active_plans'])): ?>
                                                <?php $__currentLoopData = $sc['active_plans']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $active): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php
                                                        $planName = str_replace(" ", '_', strtolower($active['value']));
                                                    ?>
                                                    <?php if(strtolower($active['value']) == strtolower(get_sub_field('plan_name'))): ?>
                                                        <?php
                                                            $plan_active = true;
                                                            break;
                                                        ?>
                                                    <?php else: ?>
                                                        <?php
                                                            $plan_active = false;
                                                        ?>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <?php if($plan_active == true): ?>
                                                    <span class="plan-name"><img src="<?= App\asset_path('images/icons/checkmark-blue.svg'); ?>" alt=""></span>
                                                <?php else: ?> 
                                                    <span class="plan-name"></span>
                                                <?php endif; ?>
                                            <?php else: ?> 
                                                <span class="plan-name"></span>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <?php
                                        $count += 1;
                                    ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <?php
                            $counter += 1;
                        ?>
                    <?php endwhile; ?>
                    <div class="currency-disclaimer currency-disclaimer--mobile">
                        <?php echo e(__('All Prices in USD', 'freshpress-theme')); ?>

                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>
</div>
