<?php
  $custom_content = get_sub_field('featured_in')['featured_logos'];
  $title = $custom_content ? get_sub_field('featured_in')['featured_label'] : get_field('featured_in_label', 'option');

  $custom_toggle = get_sub_field('custom_content');
  if ($custom_toggle && get_sub_field('featured_logos')) {
      $custom_content = get_sub_field('featured_logos');
      $title = get_sub_field('featured_label');
  }
?>

<div class="featured-in container">
    <section>
        <span class="copy"><?php echo e($title); ?></span>
        <?php if(!$custom_content): ?>
            <?php while( have_rows('featured_in_logos', 'option')): ?>
                <?php (the_row()); ?>
                <?php ($img = get_sub_field('logo')); ?>
                <?php ($logo_custom_class = get_sub_field('featured_in_logo_custom_class')); ?>
                <?php ($display = get_sub_field('featured_logo_display')); ?>
                <?php echo $__env->make('partials.components.global-image', ['img' => $img, 'classes' => 'logo ' . 'hide-' . $display . ' ' . $logo_custom_class], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endwhile; ?>
        <?php else: ?>
            <?php $__currentLoopData = $custom_content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php ($img = $value['logo']); ?>
                <?php ($logo_custom_class = isset($value['featured_in_logo_custom_class']) ? $value['featured_in_logo_custom_class'] : ''); ?>
                <?php ($display = $value['featured_logo_display']); ?>
                <?php echo $__env->make('partials.components.global-image', ['img' => $img, 'classes' => 'logo hide-' . $display . ' ' . $logo_custom_class], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </section>
</div>
