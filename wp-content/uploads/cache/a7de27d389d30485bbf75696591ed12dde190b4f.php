<div class="container globalFooter">
    <div class="footer-main">
        <?php echo $__env->make('partials.components.country-selection-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php if(have_rows('footer_social_media', 'option')): ?>
            <div class="social-copyright">
                <div class="mobile-apps-footer">
                    <div class="mobile-icons-footer">
                        <?php echo $__env->make('partials.components.global-mobile-apps', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
                <div class="social-icons">
                    <?php while(have_rows('footer_social_media', 'option')): ?>
                        <?php (the_row()); ?>
                        <a href="<?php echo e(get_sub_field('footer_social_media_url')); ?>" class="social-icon" target="_blank" rel="nofollow noopener" title="<?php echo e(get_sub_field('footer_social_media_name')); ?>">
                            <img src="<?php echo e(get_sub_field('footer_social_media_icon')['url']); ?>" alt="<?php echo e(get_sub_field('footer_social_media_icon')['alt']); ?>">
                        </a>
                    <?php endwhile; ?>
                </div>
        <?php endif; ?>
                <div class="footer-copy copyright desktop" x-ms-format-detection="none">
                    <span>
                        <?php if(get_field("enable_custom_footer_text") == "1"): ?>
                            <?php echo e(get_field('footer_custom_text')); ?>

                        <?php else: ?>
                            <?php ($field_name = App\is_uk(get_the_ID()) ? 'footer_copyright_text_uk' : 'footer_copyright_text'); ?>
                            <?php if($footer_string = get_field($field_name, 'option')): ?>
                                <?php echo e(str_replace('#!year!#', date("Y"), $footer_string)); ?>

                            <?php endif; ?>
                        <?php endif; ?>
                    </span>
                </div>
        <?php if(have_rows('footer_social_media', 'option')): ?>
            </div>
        <?php endif; ?>
        <img src="<?= App\asset_path('images/navigation/default-logo.svg'); ?>" alt="FreshBooks Cloud Accounting" class="footer-logo">
    </div>

    <?php if(
        App\is_post_type('invoice_templates') ||
        is_tax('invoice_templates_categories') ||
        $page_template == "invoice-templates.blade.php" ||
        $page_template == "invoice-templates-flexible.blade.php" ||
        App\is_post_type('as_v2_cpt') ||
        is_tax('as_v2_categories') ||
        $page_template === "accounting-software-v2.blade.php" ||
        $page_template === 'accounting-templates-page.blade.php' ||
        App\is_post_type('accounting_templates') ||
        is_tax('accounting_templates_categories')
    ): ?>
        <?php echo $__env->make('partials.invoice-templates.invoice-templates-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php elseif($page_template === "brand-campaign.blade.php"): ?>
        <?php echo $__env->make('partials.brand-campaign.single-region-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php elseif($page_template === "info-hub-page.blade.php"): ?>
        <?php echo $__env->make('partials.components.footers.global-i18l', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
</div>
