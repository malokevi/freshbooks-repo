<!doctype html>
<html <?php (language_attributes()); ?>>
    <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php
    $bodyClasses = [];
    if (get_field('full_width_layout')) {
        $bodyClasses[] = 'fullWidthLayout';
    }
    if (get_field('custom_body_class')) {
        $bodyClasses[] = get_field('custom_body_class');
    }
    ?>
    <body <?php (body_class(implode(' ', $bodyClasses))); ?>>
        <?php if(defined('WP_ENV') && WP_ENV  == 'production'): ?> <?php echo $__env->make('partials/GTM-Body-Tag', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php endif; ?>
        <?php echo $__env->make('partials.global.android-banner-smart', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php (do_action('get_header')); ?>
        <?php if(!is_404()): ?>
            <?php echo $__env->make('partials.global.banners', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('partials.global.cta-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('partials.global.cta-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
        <?php echo $__env->make('partials.country-selection-banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="main-container">
            <div class="overlay"></div>
            <main class="main">
                <?php echo $__env->yieldContent('content'); ?>
            </main>
        </div>
        <?php (do_action('get_footer')); ?>
        <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('partials/global/notification-banners/cookies-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php (wp_footer()); ?>
    </body>
</html>
