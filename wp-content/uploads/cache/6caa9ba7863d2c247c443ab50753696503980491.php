<?php

/**
* @param $count from flexible-content.blade.php used for styling
*/

global $post;
$post_id = $post->ID;

$layouts = get_post_meta($post_id, 'flexible_content');
$layout_counts = array_count_values($layouts[0]);
$total_layouts = $layout_counts['two_col_image_content'];
$section = isset($count) ? 'section' . $count : '';
$headingWeight = get_sub_field('heading_weight') ? get_sub_field('heading_weight') : 'h2' ;
$icon = get_sub_field('content_header_image');
?>

<?php ($title = get_sub_field('title') ? strtolower(str_replace(' ', '', get_sub_field('title'))) : 'cpy-feature-cta'); ?>

<div class="container container--body two-col-image-content anchored-content <?php echo e(get_sub_field('custom_pad_class')); ?> content-<?php echo e(get_sub_field('layout')); ?> <?php echo e(isset($count) ? ($count == 1 ? "first-container" : ($count == $total_layouts ? "last-container" : "")) : ''); ?>">
	<section>
		<div class="two-col">
			<div class="col text-col">
				<?php if(get_sub_field('two-col-content_header')): ?>
					<div class="title__block">
						<?php if(get_sub_field('content_header_image')): ?>
							<?php echo $__env->make('partials.components.global-image', ['img' => $icon, 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						<?php endif; ?>
						<<?php echo e($headingWeight); ?>><?php echo get_sub_field('two-col-content_header'); ?></<?php echo e($headingWeight); ?>>
					</div>
				<?php endif; ?>
				<?php echo get_sub_field('two-col-content'); ?>

				<?php if(get_sub_field('content_include')): ?>
					<?php ($includeContent = get_sub_field('content_include')); ?>
                    <?php if($includeContent == 'cta'): ?>
						<?php if(get_sub_field('cta')): ?>
							<?php echo $__env->make('partials.components.global-link', ['btn' => get_sub_field('cta')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						<?php endif; ?>
                    <?php elseif($includeContent == 'mobile'): ?>
						<div class="mobile-icons-download-section">
							<?php echo $__env->make('partials.components.global-mobile-apps', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</div>
                    <?php endif; ?>
                <?php endif; ?>
			</div>
			<div class="col img-col">
				<?php if(get_sub_field('enable_tabs')): ?>
					<?php if(have_rows('two-col_tab_content')): ?>
						<div class="payment-tab-content">
							<div class="tabs">
								<?php while(have_rows('two-col_tab_content')): ?> <?php (the_row()); ?>
									<?php ($row = get_row_index()); ?>
									<a href="#" class="btn-content-toggle tab <?php echo e($row == 1 ? "active" : ""); ?>" data-target="tab-<?php echo e($row); ?>"><?php echo e(get_sub_field('tab_title')); ?></a>
								<?php endwhile; ?>
							</div>
							<div class="payment-infos">
								<?php while(have_rows('two-col_tab_content')): ?> <?php (the_row()); ?>
									<div class="tab-<?php echo e(get_row_index()); ?> payment-info">
										<?php echo get_sub_field('tab_content'); ?>

									</div>
								<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php elseif(get_sub_field('two-col-image')): ?>
					<?php echo $__env->make('partials.components.global-image', ['img' => get_sub_field('two-col-image'), 'classes' => 'content-img ' . (get_sub_field('image_no_padding') ? 'no-pad' : '') . (get_sub_field('custom_image_class') ? get_sub_field('custom_image_class') : ''), 'lazy_load' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>
