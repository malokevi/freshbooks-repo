<?php
    $banners = App\getBannersCached();
    $loadAssets = false;
?>
<?php if(!empty($banners)): ?>
    <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $active = componentIsActive($banner['banner_settings'], get_queried_object_id());
            $loadAssets = $active || $loadAssets;

            $bannerId = 'banner-' . $banner['banner_settings']['banner_id'];

            $bannerVars = [
                'bid'      => $bannerId,
                'settings' => $banner['banner_settings'],
                'content'  => $banner['banner_content'],
                'cookie'   => isset($_COOKIE[$bannerId . '-dismissed']),
            ];
        ?>
        <?php if($active): ?>
            <?php echo $__env->make('partials.global.notification-banners.' . $banner['banner_settings']['banner_style'], $bannerVars, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php if($loadAssets): ?>
        <?php
            wp_enqueue_style('banners', App\asset_path('styles/banners.css'), false, null);
            wp_enqueue_script('banners', App\asset_path('scripts/banners.js'), ['jquery'], null, true);
        ?>
    <?php endif; ?>
<?php endif; ?>
