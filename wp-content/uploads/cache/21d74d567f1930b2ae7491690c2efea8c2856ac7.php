<div class="smartbanner hide display-control">
    <div class="smartbanner-container">
        <a href="javascript:;" class="smartbanner-close">&times;</a>
        <?php if(get_field('banner_app_icon', 'option')): ?>
            <span class="smartbanner-icon">
                <img src="<?php echo e(get_field('banner_app_icon', 'option')['url']); ?>" alt="<?php echo e(get_field('banner_app_icon', 'option')['alt']); ?>" />
            </span>
        <?php endif; ?>
        <div class="smartbanner-info">
            <?php if(get_field('banner_title', 'option')): ?>
                <div class="smartbanner-title"><?php echo e(get_field('banner_title', 'option')); ?></div>
            <?php endif; ?>    
            <?php if(get_field('banner_app_subtitle', 'option')): ?>
                <div><?php echo e(get_field('banner_app_subtitle', 'option')); ?></div>
            <?php endif; ?>  
            <?php if(get_field('banner_app_info', 'option')): ?>
                <span><?php echo e(get_field('banner_app_info', 'option')); ?></span>
            <?php endif; ?>  
        </div>
        <?php if(get_field('banner_app_url', 'option')): ?>
            <a href="<?php echo e(get_field('banner_app_url', 'option')); ?>" class="smartbanner-button">
                <span class="smartbanner-button-text"><?php echo e(get_field('banner_cta_text', 'option')); ?></span>
            </a>
        <?php endif; ?>
    </div>
</div>
    