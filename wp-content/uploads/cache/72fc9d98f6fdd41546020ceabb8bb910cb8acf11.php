<?php
    $i18nEnabled = App\isI18nEnabled();
    $allRegions = App\getAllRegions();
    $hideCountrySelectionBanner = App\isCountryBannerHidden();
?>
<?php if($i18nEnabled && !empty($allRegions) && !$hideCountrySelectionBanner): ?>
    <div class="countrySelectionBanner hidden">
        <div class="countrySelectionBanner__content">

            <div class="countrySelectionBanner__content--copy">
                <?php echo get_field('international_banner_text', 'option'); ?>

            </div>

            <div class="countrySelectionBanner__inputs">
                <div class="countrySelectionBanner__select fbSelect">
                    <select class="customSelect">
                        <option disabled selected>Select your region</option>
                        <?php $__currentLoopData = $allRegions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countryCode => $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    $optionName = count($languages) > 1 ? $lang['fullName'] : $lang['countryName'];
                                ?>
                                <option value="<?php echo e($lang['fullCode']); ?>"><?php echo e($optionName); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>

            <a href="#" class="countrySelectionBanner__close btn-close" tabindex="0">
                <img class="countrySelectionBanner__closeIcon" src="<?= App\asset_path('images/icons/close-dark.svg'); ?>" alt="<?php echo e(_x('Close banner', 'Region banner', 'freshpress-theme')); ?>">
            </a>
        </div>
    </div>
<?php endif; ?>
